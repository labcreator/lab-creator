package com.chemistry.dao;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.chemistry.controller.Student;
import com.chemistry.dao.users.StudentDAO;
import com.chemistry.dao.users.StudentQuestionsDAO;
import com.chemistry.model.SelectExperiment;
import com.chemistry.service.StudentService;
import com.google.gson.Gson;

public class StudentDAOTest {

	StoreProcess process = new StoreProcess();
	StudentQuestionsDAO student1 = new StudentQuestionsDAO();
	StudentDAO studentDAO = new StudentDAO();
	StudentService service = new StudentService();
	Student student = new Student();
	SelectExperiment experiment = null;
	
	@Before
	public void setUp() throws Exception {
		student1.setProcess(process);
		studentDAO.setStudent(student1);
		experiment = studentDAO.getSelectedExperiment(1);
		
	/*	System.out.println("Exp Name : "+experiment.getName());
		System.out.println("Prelab size : "+experiment.getPrelab().size());
		System.out.println("Lab size : "+experiment.getLab().size());
		System.out.println("Postlab size : "+experiment.getPostlab().size());	*/	
	}

	@Test
	public void test() {
		
		service.setStudent(studentDAO);
		experiment = service.getExperiment(1);
		
		System.out.println("Exp Name : "+experiment.getName());
		System.out.println("Prelab size : "+experiment.getPrelab().size());
		System.out.println("Lab size : "+experiment.getLab().size());
		System.out.println("Postlab size : "+experiment.getPostlab().size());		

/*		student.setStudent(service);
		String s = student.requestPrelabSection();*/
	
		System.out.println("Prelab : "+new Gson().toJson(experiment.getPrelabModName()));
		System.out.println("Lab : "+new Gson().toJson(experiment.getLabModName()));
		System.out.println("PostLab : "+new Gson().toJson(experiment.getPostlabModName()));
		
	//	System.out.println("Prelab : "+s);
//		fail("Not yet implemented");
	}

	@After
	public void tearDown() throws Exception {
	}


}
