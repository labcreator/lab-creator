package com.chemistry.json.instructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.enums.QuestionMember;
import com.chemistry.enums.QuestionType;
import com.chemistry.enums.SectionType;
import com.chemistry.model.Module;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.Question;
import com.chemistry.model.questions.ShortBlank;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@Service
@Scope("session")
public class JsonAssignment {

	static int pre = 1;
	static int pos = 1;
	static int lab = 1;
	
	static HashMap<Long, Question> prelab = new HashMap<Long, Question>();
	static HashMap<Long, Question> labs = new HashMap<Long, Question>();
	static HashMap<Long, Question> postlab = new HashMap<Long, Question>();
	
	static HashMap<Long, Module> modules = new HashMap<Long, Module>();
	
	private QuestionMember memeber = new QuestionMember();
	
	
	int i;
	
	Map<Module,List<Question>> map = new HashMap<Module, List<Question>>();
	QuestionMember member = new QuestionMember();
	
	/*public void jsonToAssignment(JsonObject assignment) {
		
		JsonPrimitive obj2 =  assignment.getAsJsonPrimitive("assignmentName");
		System.out.println("Name : "+obj2.getAsString());
		JsonArray array = assignment.getAsJsonArray("modules");		
		System.out.println("Modules size : "+array.size());
		
		JsonArray questions = assignment.getAsJsonArray("assignmentquestions");
		System.out.println("Questions size : "+questions.size());
		
		setModules(array);
		//setAllQuestions(questions);
		
		// encode data on your side using BASE64
		byte[]   bytesEncoded = Base64.encodeBase64(str .getBytes());
		System.out.println("ecncoded value is " + new String(bytesEncoded ));
	}
	
	private void setModules(JsonArray array) {
		for(int i = 0; i < array.size(); i++) {
			JsonObject obj = (JsonObject) array.get(i);
			
			JsonElement ele = obj.get("question");
			JsonElement ele2 = obj.get("fname");
			JsonElement ele3 = obj.get("id");
			
			String ques  = ele.getAsString();
			String name = ele2.getAsString();
			long id = ele3.getAsLong();
	
			String moduleName = obj.get("question").getAsString();
			long id = obj.get("id").getAsLong();
			
			Module module = new Module();
			module.setId(id);
		//	module.setName(moduleName);
			
			map.put(module, null);
		}
	}*/
	
	
	
	public Map<Long,Question> getPrelab() {
		return prelab;
	}
	
	public Map<Long, Module> getModules() {
		return modules;
	}
	
	public Module jsonModule(JsonObject object) {
		
		Module module = new Module();
		module.setId(object.get("id").getAsLong());
		module.setModuleName(object.get("question").getAsString());
		module.setFileName(object.get("fname").getAsString());
		
		modules.put(module.getId(), module);
		
		return module;
	}
	
	public Module jsonEditModule(JsonObject object) {
		
		Module module = new Module();
		module.setId(object.get("id").getAsLong());
		module.setModuleName(object.get("question").getAsString());
		module.setFileName(object.get("fname").getAsString());
		
		Module oldModule = modules.get(module.getId());
		if(oldModule != null)
			module.setId(oldModule.getId());
		
		modules.remove(module.getId());
		modules.put(module.getId(), module);
		
		return module;
	}
	
	public Question jsonEditQuestion(JsonObject object) {
		long id = object.get("id").getAsLong();
		String section = object.get("section").getAsString();
		Question newQuestion = createQuestion(object, section);
		Question oldQuestion = prelab.get(id);
		if(oldQuestion != null)
			newQuestion.setQuestionNo(oldQuestion.getQuestionNo());
//		MultipleChoice m = (MultipleChoice) question;
		
		if(section.equalsIgnoreCase(member.section(SectionType.PRE))) {
			prelab.remove(id);
			prelab.put(id, newQuestion);
		} else if(section.equalsIgnoreCase(member.section(SectionType.LAB))) {
			labs.remove(id);
			labs.put(id, newQuestion);
		} else if(section.equalsIgnoreCase(member.section(SectionType.POST))) {
			postlab.remove(id);
			postlab.put(id, newQuestion);
		}	
		return newQuestion;
	}
	
	public Question jsonQuestion(JsonObject object) {
		String section = object.get("section").getAsString();
		Question question = createQuestion(object, section);
	//	MultipleChoice m = (MultipleChoice) question;
		if(section.equalsIgnoreCase(member.section(SectionType.PRE))) {
			question.setQuestionNo(pre++);
			prelab.put(question.getQuestionId(), question);
		} else if(section.equalsIgnoreCase(member.section(SectionType.LAB))) {
			question.setQuestionNo(lab++);
			labs.put(question.getQuestionId(), question);
		} else if(section.equalsIgnoreCase(member.section(SectionType.POST))) {
			question.setQuestionNo(pos++);
			postlab.put(question.getQuestionId(), question);			
		}	
		return question;
	}
	
	private Question createQuestion(JsonObject object,  String section) {
		String type = object.get("type").getAsString();
		Question question = null;
		if(type.equalsIgnoreCase(member.type(QuestionType.MC))) {
			question = setMultipleChoice(object);
		} else if(type.equalsIgnoreCase(member.type(QuestionType.NUM))) {
			question = setNumerical(object);
		} else if(type.equalsIgnoreCase(member.type(QuestionType.SHORT))) {
			question = setShort(object);
		} else if(type.equalsIgnoreCase(member.type(QuestionType.DATA))) {
			question = setDataEntry(object);
		}
		
		return question;
	}
	
	
	private Question setDataEntry(JsonObject object) {
		DataEntry entry = new DataEntry();
		entry.setQuestionId(object.get("id").getAsLong());
		entry.setModuleId(object.get("modId").getAsLong());
		entry.setQuestion(object.get("question").getAsString());
		entry.setSection(object.get("section").getAsString());
		entry.setQuestionType(object.get("type").getAsString());
		entry.setUnits(object.get("units").getAsString());
		entry.setRows(object.get("rows").getAsInt());
		entry.setColumns(object.get("columns").getAsInt());
		entry.setColumnName(object.get("columnNames").getAsString());
		return entry;
	}
	
	private Question setMultipleChoice(JsonObject object) {
		MultipleChoice multiple = new MultipleChoice();
		multiple.setQuestionId(object.get("id").getAsLong());
		multiple.setModuleId(object.get("modId").getAsLong());
		multiple.setQuestion(object.get("question").getAsString());
		multiple.setAnswer(object.get("cAns").getAsString());
		multiple.setSection(object.get("section").getAsString());
		multiple.setQuestionType(object.get("type").getAsString());
		multiple.setIncorrect(object.get("i1").getAsString(), 0);
		multiple.setIncorrect(object.get("i2").getAsString(), 1);
		multiple.setIncorrect(object.get("i3").getAsString(), 2);
		multiple.setHint(object.get("h1").getAsString(), 0);
		multiple.setHint(object.get("h2").getAsString(), 1);
		multiple.setHint(object.get("h3").getAsString(), 2);
		return multiple;
	}
	
	private Question setNumerical(JsonObject object) {
		Numerical numerical = new Numerical();
		numerical.setQuestionId(object.get("id").getAsLong());
		numerical.setModuleId(object.get("modId").getAsLong());
		numerical.setQuestion(object.get("question").getAsString());
		numerical.setAnswer(object.get("cAns").getAsString());
		numerical.setSection(object.get("section").getAsString());
		numerical.setQuestionType(object.get("type").getAsString());
		numerical.setHint(object.get("h1").getAsString(), 0);
		numerical.setHint(object.get("h2").getAsString(), 1);
		numerical.setHint(object.get("h3").getAsString(), 2);
		numerical.setFromRange(object.get("r1").getAsDouble());
		numerical.setToRange(object.get("r2").getAsDouble());
		return numerical;
	}
	
	private Question setShort(JsonObject object) {
		ShortBlank blank = new ShortBlank();
		blank.setQuestionId(object.get("id").getAsLong());
		blank.setModuleId(object.get("modId").getAsLong());
		blank.setQuestion(object.get("question").getAsString());
		blank.setAnswer(object.get("cAns").getAsString());
		blank.setSection(object.get("section").getAsString());
		blank.setQuestionType(object.get("type").getAsString());
		return blank;
	}
	
	public void JsonAssignmentQuestions(JsonObject object) {
//		JsonObject obj = object.get
	}
	
	private void setExperiment(JsonObject object) {
		System.out.println("Setting Experiment");
	}
}
