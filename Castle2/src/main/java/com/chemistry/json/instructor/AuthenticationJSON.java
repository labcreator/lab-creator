package com.chemistry.json.instructor;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.Registration;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@Service
@Scope("session")
public class AuthenticationJSON {

	public Login loginJSON(JsonObject object) {
		
		JsonPrimitive obj1 = object.getAsJsonPrimitive("netid");
		JsonPrimitive obj2 = object.getAsJsonPrimitive("name");
		JsonPrimitive obj3 = object.getAsJsonPrimitive("usertype");
		
		Login login = new Login(obj1.getAsString(),
								obj2.getAsString(),
								obj3.getAsString());
		
		login.toString();
		
		return login;
	}
	
	public Registration registerJSON(JsonObject object) {
		
		JsonPrimitive obj1 = object.getAsJsonPrimitive("netid");
		JsonPrimitive obj2 = object.getAsJsonPrimitive("fname");
		JsonPrimitive obj3 = object.getAsJsonPrimitive("lname");
		
		Registration register = new Registration(obj1.getAsString(),
												 obj2.getAsString(),
												 obj3.getAsString());
		
		register.toString();
		
		return register;
	}
}
