package com.chemistry.dao.users;

import java.lang.reflect.Member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.chemistry.dao.StoreProcess;
import com.chemistry.enums.QuestionMember;
import com.chemistry.enums.QuestionType;
import com.chemistry.model.Assignment;
import com.chemistry.model.Module;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.ShortBlank;

public class StudentQuestionsDAO  {

	private QuestionMember member = new QuestionMember();
	private StoreProcess process = null;
	
	
	@Autowired
	public void setProcess(StoreProcess process) {
		this.process = process;
	}

	public List<Assignment> getExperimentList() {
		return process.getExperimentList();
	}

	public Connection getConnection() {
		return process.connection;
	}
	
	//************ Database Connections ***************
	private ResultSet getResultSetQuery(String sql) throws SQLException {
		return process.getResultSetQuery(sql);
	}
	
	private int insertSQLQuery(String sql) throws SQLException {
		return process.insertSQLQuery(sql);
	}
	
	private int updateSQLQuery(String sql) throws SQLException {
		return process.updateSQLQuery(sql);
	}

	private int deleteSQLQuery(String sql) throws SQLException {
		return updateSQLQuery(sql);
	}
	
	//************** Retrieve Operations *************
	public String getExperimentName(int id) {
		String sql = "select * from experiment where id = "+id;
		
		try {
			ResultSet rs = getResultSetQuery(sql);
			if(rs.next())
				return rs.getString("name");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Module> getModules(int id) {
		
/*		String sql = "select * from module, experimentquestions "
				+ "where module.id = experimentquestions.moduleid and experimentquestions.experimentid = "+id;*/
		
		String sql = "select * from module where module.id in "
				+ "(select experimentquestions.moduleid from experimentquestions "
				+ "where experimentquestions.experimentid = "+id+")";
		
		List<Module> list = new ArrayList<Module>();
		
		try {
			ResultSet rs = getResultSetQuery(sql);
			while(rs.next()) {
				Module module = new Module();
				module.setId(rs.getLong("module.id"));
				module.setModuleName(rs.getString("module.name"));
				list.add(module);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public List<MultipleChoice> getMultipleChoice(long moduleid, long experid) {
		
/*		String sql = "select * from multiplechoice, experimentquestions "
				+ "where multiplechoice.id = experimentquestions.mc_id and "
				+ "experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid = "+moduleid;*/

		String sql = "select * from multiplechoice where multiplechoice.id in "
				+ "(select experimentquestions.mc_id from experimentquestions "
				+ "where experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid="+moduleid+")";
		
		List<MultipleChoice> list = new ArrayList<MultipleChoice>();
        
		System.out.println("MC SQL : "+sql);
		try {
			ResultSet rs = getResultSetQuery(sql);
			list = setResultSetMultipleChoices(rs, moduleid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public List<DataEntry> getDataEntry(long moduleid, long experid) {
		
/*		String sql = "select * from dataentry, experimentquestions "
				+ "where dataentry.id = experimentquestions.de_id and "
				+ "experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid = "+moduleid;*/

		String sql = "select * from dataentry where dataentry.id in "
				+ "(select experimentquestions.de_id from experimentquestions "
				+ "where experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid="+moduleid+")";
		
		List<DataEntry> list = new ArrayList<DataEntry>();
        
		try {
			ResultSet rs = getResultSetQuery(sql);
			list = setResultSetDataEntries(rs, moduleid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public List<Numerical> getNumerical(long moduleid, long experid) {
		
/*		String sql = "select * from numerical, experimentquestions "
				+ "where numerical.id = experimentquestions.num_id and "
				+ "experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid = "+moduleid;*/
		
		String sql = "select * from numerical where numerical.id in "
				+ "(select experimentquestions.num_id from experimentquestions "
				+ "where experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid="+moduleid+")";

		List<Numerical> list = new ArrayList<Numerical>();
        
		System.out.println("NUM SQL : "+sql);
		
		try {
			ResultSet rs = getResultSetQuery(sql);
			list = setResultSetNumericals(rs, moduleid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public List<ShortBlank> getShortBlank(long moduleid, long experid) {
		
/*		String sql = "select * from short, experimentquestions "
				+ "where short.id = experimentquestions.short_id and "
				+ "experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid = "+moduleid;*/
		
		String sql = "select * from short where short.id in "
				+ "(select experimentquestions.short_id from experimentquestions "
				+ "where experimentquestions.experimentid = "+experid+" and experimentquestions.moduleid="+moduleid+")";

		List<ShortBlank> list = new ArrayList<ShortBlank>();
        
		try {
			ResultSet rs = getResultSetQuery(sql);
			list = setResultSetShortBlanks(rs, moduleid);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}

	private List<DataEntry> setResultSetDataEntries(ResultSet rs, long moduleid) throws SQLException {
		List<DataEntry> list = new ArrayList<DataEntry>();
        
		while(rs.next()) {
			DataEntry entry = new DataEntry();
			entry.setQuestionId(rs.getLong("id"));
			entry.setQuestion(rs.getString("question"));
			entry.setSection(rs.getString("section"));
			entry.setQuestionNo(rs.getInt("qno"));
			entry.setColumns(rs.getInt("columns"));
			entry.setRows(rs.getInt("rows"));
			entry.setColumnName(rs.getString("columnnames"));
			entry.setUnits(rs.getString("units"));
			entry.setModuleId(moduleid);
			entry.setQuestionType(member.type(QuestionType.DATA));
			list.add(entry);
		}
		return list;
	}
	
	private List<ShortBlank> setResultSetShortBlanks(ResultSet rs, long moduleid) throws SQLException {
		List<ShortBlank> list = new ArrayList<ShortBlank>();
	       
		while(rs.next()) {
			ShortBlank blank = new ShortBlank();
			blank.setQuestionId(rs.getLong("id"));
			blank.setQuestion(rs.getString("question"));
			blank.setAnswer(rs.getString("answer"));
			blank.setSection(rs.getString("section"));
			blank.setQuestionNo(rs.getInt("qno"));
			blank.setModuleId(moduleid);
			blank.setQuestionType(member.type(QuestionType.SHORT));
			list.add(blank);
		}
		return list;
	}
	
 	private List<Numerical> setResultSetNumericals(ResultSet rs, long moduleid) throws NumberFormatException, SQLException {
 		List<Numerical> list = new ArrayList<Numerical>();
        
		while(rs.next()) {
			Numerical numerical = new Numerical();
			numerical.setQuestionId(rs.getLong("id"));
			numerical.setQuestion(rs.getString("question"));
			numerical.setAnswer(rs.getString("answer"));
			numerical.setSection(rs.getString("section"));
			numerical.setQuestionNo(rs.getInt("qno"));
			numerical.setFromRange(Double.parseDouble(rs.getString("fromrange")));
			numerical.setToRange(Double.parseDouble(rs.getString("torange")));
			numerical.setHint(rs.getString("hint1"), 0);
			numerical.setHint(rs.getString("hint2"), 1);
			numerical.setHint(rs.getString("hint3"), 2);
			numerical.setQuestionType(member.type(QuestionType.NUM));
			numerical.setModuleId(moduleid);
			list.add(numerical);
		}
		return list;
	}
		
	private List<MultipleChoice> setResultSetMultipleChoices(ResultSet rs, long moduleid) throws SQLException {

			List<MultipleChoice> list = new ArrayList<MultipleChoice>();
            
			while(rs.next()) {
				MultipleChoice choice = new MultipleChoice();
				choice.setQuestionId(rs.getLong("id"));
				choice.setQuestion(rs.getString("question"));
				choice.setAnswer(rs.getString("answer"));
				choice.setSection(rs.getString("section"));
				choice.setQuestionNo(rs.getInt("qno"));
				choice.setIncorrect(rs.getString("invalid1"), 0);
				choice.setIncorrect(rs.getString("invalid2"), 1);
				choice.setIncorrect(rs.getString("invalid3"), 2);
				choice.getInvalid().add(choice.getIncorrect(0));
				choice.getInvalid().add(choice.getIncorrect(1));
				choice.getInvalid().add(choice.getIncorrect(2));
				choice.setHint(rs.getString("hint1"), 0);
				choice.setHint(rs.getString("hint2"), 1);
				choice.setHint(rs.getString("hint3"), 2);
				choice.setModuleId(moduleid);
				choice.setQuestionType(member.type(QuestionType.MC));
				list.add(choice);
			}
			return list;
		}
	
	}

