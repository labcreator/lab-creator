package com.chemistry.dao.users;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.chemistry.enums.QuestionMember;
import com.chemistry.enums.QuestionType;
import com.chemistry.enums.SectionType;
import com.chemistry.model.Assignment;
import com.chemistry.model.Experiment;
import com.chemistry.model.Module;
import com.chemistry.model.SelectExperiment;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.Question;
import com.chemistry.model.questions.ShortBlank;
import com.chemistry.model.util.UtilitySection;

@Repository
@Scope("session")
public class StudentDAO {
	
	
	private List<Module> modules = new ArrayList<Module>();
	private List<Assignment> experimentList = null;
	private List<Question> allQuestions = new ArrayList<Question>();
	private Map<Module, List<Question>> prelab = new HashMap<Module, List<Question>>();
	private Map<Module, List<Question>> lab = new HashMap<Module, List<Question>>();
	private Map<Module, List<Question>> postlab = new HashMap<Module, List<Question>>();
	
	private Map<String, List<Question>> prelabModName = new HashMap<String, List<Question>>();
	private Map<String, List<Question>> labModName = new HashMap<String, List<Question>>();
	private Map<String, List<Question>> postlabModName = new HashMap<String, List<Question>>();
	
	private SelectExperiment selectedExperiment = null;
	private StudentQuestionsDAO student = null;
	
	private QuestionMember member = new QuestionMember();
	
	@Autowired
	public void setStudent(StudentQuestionsDAO student) {
		this.student = student;
	}

	public List<Module> getModules() {
		return modules;
	}

	private void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Question> getAllQuestions() {
		return allQuestions;
	}

	private void setAllQuestions(List<Question> allQuestions) {
		this.allQuestions = allQuestions;
	}

	public Map<Module, List<Question>> getPrelab() {
		return prelab;
	}

	private void setPrelab(Map<Module, List<Question>> prelab) {
		this.prelab = prelab;
	}

	public Map<Module, List<Question>> getLab() {
		return lab;
	}

	private void setLab(Map<Module, List<Question>> lab) {
		this.lab = lab;
	}

	public Map<Module, List<Question>> getPostlab() {
		return postlab;
	}

	private void setPostlab(Map<Module, List<Question>> postlab) {
		this.postlab = postlab;
	}

	public SelectExperiment getSelectedExperiment() {
		return selectedExperiment;
	}

	public List<Assignment> getExperimentList() {
		return student.getExperimentList();
	}
	
	
	public Map<String, List<Question>> getPrelabModName() {
		return prelabModName;
	}

	public void setPrelabModName(Map<String, List<Question>> prelabModName) {
		this.prelabModName = prelabModName;
	}

	public Map<String, List<Question>> getLabModName() {
		return labModName;
	}

	public void setLabModName(Map<String, List<Question>> labModName) {
		this.labModName = labModName;
	}

	public Map<String, List<Question>> getPostlabModName() {
		return postlabModName;
	}

	public void setPostlabModName(Map<String, List<Question>> postlabModName) {
		this.postlabModName = postlabModName;
	}

	public SelectExperiment getSelectedExperiment(int id) {
		String name = student.getExperimentName(id);
		SelectExperiment experiment = new SelectExperiment();
		experiment.setName(name);
		
		this.modules = student.getModules(id);
		experiment.setModules(this.modules);
		
		if(!setExperimentSections(this.modules, id)) 		
			return null;
		
		experiment.setPrelab(this.prelab);
		experiment.setLab(this.lab);
		experiment.setPostlab(this.postlab);
		
		experiment.setPrelabModName(this.prelabModName);
		experiment.setLabModName(this.labModName);
		experiment.setPostlabModName(this.postlabModName);
		
		this.selectedExperiment = experiment;
		
		return experiment;
	}
	
	private boolean setExperimentSections(List<Module> modules, int id) {
		
		List<Question> allQuestions = new ArrayList<Question>();	
		Iterator<Module> iterate = modules.iterator();
		
		System.out.println("Modules Size : "+modules.size());
		try {
			while(iterate.hasNext()) {
				Module module = iterate.next();
				allQuestions = new ArrayList<Question>();
				List<MultipleChoice> mc = student.getMultipleChoice(module.getId(), id);
//				System.out.println("MC List : "+mc.size());
				allQuestions.addAll(mc);
				
				List<Numerical> num = student.getNumerical(module.getId(), id);
//				System.out.println("NUM List : "+num.size());
				allQuestions.addAll(num);
				
				List<ShortBlank> bla = student.getShortBlank(module.getId(), id);
//				System.out.println("SB List : "+bla.size());
				allQuestions.addAll(bla);
				
				List<DataEntry> ent = student.getDataEntry(module.getId(), id);
//				System.out.println("ENT List : "+ent.size());
				allQuestions.addAll(ent);
				
//				System.out.println("All Questions Size : "+allQuestions.size());
				UtilitySection section = getSectionQuestions(allQuestions);
				
	/*			System.out.println(" "+section.getPrelab().size());
				System.out.println(" "+section.getLab().size());
				System.out.println(" "+section.getPostlab().size());*/
				
				this.prelab.put(module, section.getPrelab());
				this.lab.put(module, section.getLab());
				this.postlab.put(module, section.getPostlab());
				
				this.prelabModName.put(module.getModuleName(), section.getPrelab());
				this.labModName.put(module.getModuleName(), section.getLab());
				this.postlabModName.put(module.getModuleName(), section.getPostlab());
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private UtilitySection getSectionQuestions(List<Question> list) {
		
		//List<Question> questions = new ArrayList<Question>();
		Iterator<Question> iterate = list.iterator();
		List<Question> prelab = new ArrayList<Question>();
		List<Question> lab = new ArrayList<Question>();
		List<Question> postlab = new ArrayList<Question>();
			
		UtilitySection utility = new UtilitySection();
		
		while(iterate.hasNext()) {
			Question question = iterate.next();
	//		System.out.println(question.getQuestionType());
			if(question.getSection().equalsIgnoreCase(member.section(SectionType.PRE))) {
	//			System.out.println("In Prelab : "+question.getQuestionNo());
				prelab.add(question);
				prelab = sortSectionQuestions(prelab);
			} else if(question.getSection().equalsIgnoreCase(member.section(SectionType.LAB))) {
				lab.add(question);
				lab = sortSectionQuestions(lab);
			} else if(question.getSection().equalsIgnoreCase(member.section(SectionType.POST))) {
				postlab.add(question);
				postlab = sortSectionQuestions(postlab);
			}
		}
		
		utility.setPrelab(prelab);
		utility.setLab(lab);
		utility.setPostlab(postlab);
		
		return utility;
	}
	
	private List<Question> sortSectionQuestions(List<Question> list) {
		
		Collections.sort(list, new Comparator<Question>() {
            public int compare(Question p1, Question p2) {

                if(p1.getQuestionNo() > p2.getQuestionNo()) {
                    return 1;
                }
                else if(p1.getQuestionNo() < p2.getQuestionNo()) {
                    return -1;
                }
                
                return 0;
            }
        });
		
		return list;
	}

	
}
