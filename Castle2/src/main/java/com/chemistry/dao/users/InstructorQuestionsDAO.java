package com.chemistry.dao.users;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.chemistry.dao.StoreProcess;
import com.chemistry.model.Assignment;
import com.chemistry.model.Module;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.ExperimentQuestions;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.ShortBlank;

@Repository
@Scope("session")
public class InstructorQuestionsDAO extends StoreProcess {

	
	public List<Assignment> getExperimentList() {
		return super.getExperimentList();
	}
	
	// ********** Insert Experiment Name ***********
	public int insertExperimentName(String name) {
		
		String sql = "insert into experiment(name) values('"+name+"')";
		try {
			return insertSQLQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean insertModule(Module module) {
		
		String sql = "insert into module values("+module.getId()+",'"+module.getModuleName()+"')";
		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	// ************** Single Question Insertions ***************
	public boolean insertMultipleChoice(MultipleChoice choice) {
		
		String sql = "insert into multiplechoice values("+choice.getQuestionId()+","
									+ "'"+choice.getQuestion()+"',"
									+ "'"+choice.getAnswer()+"',"
									+ "'"+choice.getSection()+"',"
										+choice.getQuestionNo()+","
												+ "'"+choice.getIncorrect(0)+"',"
												+ "'"+choice.getIncorrect(1)+"',"
												+ "'"+choice.getIncorrect(2)+"',"
												+ "'"+choice.getHint(0)+"',"
												+ "'"+choice.getHint(1)+"',"
												+ "'"+choice.getHint(2)+"')";
		
		System.out.println("MC SQL : "+sql);
		
		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean insertNumerical(Numerical numerical) {
		
		String sql = "insert into numerical values("+numerical.getQuestionId()+","
				+ "'"+numerical.getQuestion()+"',"
						+ "'"+numerical.getAnswer()+"',"
								+ "'"+numerical.getSection()+"',"
										+numerical.getQuestionNo()+","
												+ "'"+numerical.getFromRange()+"',"
														+ "'"+numerical.getToRange()+"',"
																+ "'"+numerical.getHints(0)+"',"
																+ "'"+numerical.getHints(1)+"',"
																+ "'"+numerical.getHints(2)+"')";
		System.out.println("NUM SQL : "+sql);
		
		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	
	public boolean insertShortBlank(ShortBlank blank) {
		
		String sql = "insert into short values("+blank.getQuestionId()+","
				+ "'"+blank.getQuestion()+"',"
						+ "'"+blank.getAnswer()+"',"
								+ "'"+blank.getSection()+"',"
										+ blank.getQuestionNo()+")";

		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean insertDataEntry(DataEntry entry) {

		String sql = "insert into dataentry values("+entry.getQuestionId()+","
				+ "'"+entry.getQuestion()+"',"
								+ "'"+entry.getSection()+"',"
										+ entry.getQuestionNo()+","
												+ entry.getColumns()+","
														+ entry.getRows()+","
																+ "'"+entry.getColumnName()+"',"
																		+ "'"+entry.getUnits()+"')";

		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;

	}
	
	public boolean insertExperimentQuestions(ExperimentQuestions questions) {
		
		String sql = "insert into experimentquestions values("+questions.getId()+","
				+questions.getExperimentId()+","
						+ questions.getModuleId()+","
								+ questions.getMcId()+","
										+ questions.getNumId()+","
												+ questions.getShortId()+","
														+ questions.getDeId()+")";

		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	public Module getModule(long id) {
		Module module = null;
		String sql = "select * from module where id="+id;
		try {
			ResultSet rs = getResultSetQuery(sql);
			if(rs.next()) {
				module = new Module();
				module.setId(rs.getLong("id"));
				module.setModuleName("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return module;
	}
	
	// ************** Updations ***************
	public boolean updateModule(Module module) {
		
		String sql = "update module set name='"+module.getModuleName()+"' where id="+module.getId();
		
		try {
			return updateSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean updateMultipleChoice(MultipleChoice choice) {
		
		String sql = "update multiplechoice set question='"+choice.getQuestion()+"',"
									+ "answer='"+choice.getAnswer()+"',"
									+ "section='"+choice.getSection()+"',"
										+"qno="+choice.getQuestionNo()+","
												+ "invalid1='"+choice.getIncorrect(0)+"',"
												+ "invalid2='"+choice.getIncorrect(1)+"',"
												+ "invalid3='"+choice.getIncorrect(2)+"',"
												+ "hint1='"+choice.getHint(0)+"',"
												+ "hint2='"+choice.getHint(1)+"',"
												+ "hint3='"+choice.getHint(2)+"' "
														+ "where id="+choice.getQuestionId();
		
		System.out.println(sql);
		try {
			return updateSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return false;
	}

	public boolean updateNumerical(Numerical numerical) {
		
		String sql = "update numerical set question='"+numerical.getQuestion()+"',"
						+ "answer='"+numerical.getAnswer()+"',"
								+ "section='"+numerical.getSection()+"',"
										+"qno="+numerical.getQuestionNo()+","
												+ "fromrange='"+numerical.getFromRange()+"',"
														+ "torange='"+numerical.getToRange()+"',"
																+ "hint1='"+numerical.getHints(0)+"',"
																+ "hint2='"+numerical.getHints(1)+"',"
																+ "hint3='"+numerical.getHints(2)+"' "
																		+ "where id="+numerical.getQuestionId();
		
		try {
			return updateSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean updateShortBlank(ShortBlank blank) {
        
		String sql = "update short set question='"+blank.getQuestion()+"',"
						+ "answer='"+blank.getAnswer()+"',"
								+ "section='"+blank.getSection()+"',"
										+ "qno="+blank.getQuestionNo()+" "
										+ "where id="+ blank.getQuestionId();
		
		try {
			return updateSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	// ********** Deletions ***********
	public boolean deleteModule(long id) {
		
		String sql = "delete from module where id="+id;
		
		try {
			return deleteSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteModuleEQs(long id) {
 		
//		String sql1 = "DELETE FROM multiplechoice WHERE Id IN (SELECT MC_Id FROM experimentquestions WHERE moduleid = "+id+")";
		String sql2 = "delete from experimentquestions where moduleid = "+id;		
		
		try {
			//if(deleteSQLQuery(sql1) > 0)
				return deleteSQLQuery(sql2) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteModuleMultipleChoiceEQ(long id) {
         		
//		String sql1 = "DELETE FROM multiplechoice WHERE Id IN (SELECT MC_Id FROM experimentquestions WHERE moduleid = "+id+")";
		String sql2 = "delete from experimentquestions where moduleid = "+id;		
		
		try {
			//if(deleteSQLQuery(sql1) > 0)
				return deleteSQLQuery(sql2) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteModuleNumericalEQ(long id) {
        
		String sql = "delete from numerical, experimentquestions where num_id = id and moduleid="+id;

		try {
			return deleteSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public boolean deleteModuleShortBlankEQ(long id) {
        
		String sql = "delete from short, experimentquestions where short_id = id and moduleid="+id;

		try {
			return deleteSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}
	
	
	//**************** Single Question Deletion ********************
	public boolean deleteMultipleChoiceEQ(long moduleId, long id) {
		String sql1 = "delete from multiplechoice where id = "+id;
		String sql2 = "delete from experimentquestions where mc_id = "+id+" and moduleid="+moduleId;
		
		try {
			if(deleteSQLQuery(sql1) > 0)
				return deleteSQLQuery(sql2) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public boolean deleteNumericalEQ(long moduleId, long id) {
		String sql1 = "delete from numerical where id = "+id;
		String sql2 = "delete from experimentquestions where num_id = "+id+" and moduleid="+moduleId;
		
		try {
			if(deleteSQLQuery(sql1) > 0)
				return deleteSQLQuery(sql2) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public boolean deleteShortBlankEQ(long moduleId, long id) {
		String sql1 = "delete from shortblank where id = "+id;
		String sql2 = "delete from experimentquestions where short_id = "+id+" and moduleid="+moduleId;
		
		try {
			if(deleteSQLQuery(sql1) > 0)
				return deleteSQLQuery(sql2) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
