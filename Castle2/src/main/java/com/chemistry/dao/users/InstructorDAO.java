package com.chemistry.dao.users;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.chemistry.enums.QuestionMember;
import com.chemistry.enums.QuestionType;
import com.chemistry.model.Assignment;
import com.chemistry.model.Module;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.ExperimentQuestions;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.Question;
import com.chemistry.model.questions.ShortBlank;

@Repository
@Scope("session")
public class InstructorDAO {

	private InstructorQuestionsDAO instructor = null;
	private QuestionMember member = new QuestionMember();
	
	@Autowired
	public void setInstructor(InstructorQuestionsDAO instructor) {
		this.instructor = instructor;
	}
	
	public List<Assignment> getExperimentList() {
		return instructor.getExperimentList();
	}

	// *********** Multiple Iterate List *************
	public int createExperiment(String name) {
		return insertExperimentName(name);
	}
	
	public boolean createModules(Module[] modules) {
		for(int i = 0; i < modules.length; i++)
			if(!insertModule(modules[i]))
				return false;
		return true;
	}
	
	public boolean createMultipleChoice(List<MultipleChoice> questions) {
		try {
			Iterator<MultipleChoice> iterate = questions.iterator();
			while(iterate.hasNext()) {
				if(!insertMultipleChoice(iterate.next()))
					return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean createNumerical(List<Numerical> numerical) {
		try {
			Iterator<Numerical> iterate = numerical.iterator();
			while(iterate.hasNext()) {
				if(!insertNumerical(iterate.next()))
					return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean createShortBlank(List<ShortBlank> blank) {
		try {
			Iterator<ShortBlank> iterate = blank.iterator();
			while(iterate.hasNext()) {
				if(!insertShortBlank(iterate.next()))
					return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean createExperimentalQuestions(List<ExperimentQuestions> experiment) {
		try {
			Iterator<ExperimentQuestions> iterate = experiment.iterator();
			while(iterate.hasNext()) {
				if(!insertExperimentQuestions(iterate.next()))
					return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	// ************  Insert Single Entry ***************
	public int insertExperimentName(String name) {
		return instructor.insertExperimentName(name);
	}
	
	private boolean insertModule(Module module) {
		return instructor.insertModule(module);
	}

	private Module getModule(long id) {
		return instructor.getModule(id);
	}

	private boolean insertMultipleChoice(MultipleChoice choice) {
		return instructor.insertMultipleChoice(choice);
	}
	
	private boolean insertNumerical(Numerical numerical) {
		return instructor.insertNumerical(numerical);
	}
	
	private boolean insertShortBlank(ShortBlank blank) {
		return instructor.insertShortBlank(blank);
	}
	
	private boolean insertDataEntry(DataEntry entry) {
		return instructor.insertDataEntry(entry);
	}
	
	private boolean insertExperimentQuestions(ExperimentQuestions questions) {
		return instructor.insertExperimentQuestions(questions);
	}
	
	
	// ++++++++++++ Single Database Entries ***********
	public boolean createModule(Module module) {
		return insertModule(module);
	}

	public boolean addMultipleChoice(MultipleChoice choice) {
		return insertMultipleChoice(choice);
	}
	
	public boolean addNumerical(Numerical question) {
		return insertNumerical(question);
	}
	
	public boolean addShortBlank(ShortBlank question) {
		return insertShortBlank(question);
	}
	
	public boolean addDataEntry(DataEntry question) {
		return insertDataEntry(question);
	}
	
	public boolean addExperimentQuestions(ExperimentQuestions experiment) {
		return insertExperimentQuestions(experiment);
	}
	
	// **************** Update Single Question *****************
	public boolean updateModule(Module module) {
		return instructor.updateModule(module);
	}

	public boolean editMultipleChoice(MultipleChoice choice) {
		return instructor.updateMultipleChoice(choice);
	}
	
	public boolean editNumerical(Numerical numerical) {
		return instructor.updateNumerical(numerical);
	}

	public boolean editShortBlank(ShortBlank blank) {
		return instructor.updateShortBlank(blank);
	}
	
	// ************* Delete Single Questions ***********
	public boolean deleteMultipleChoice(MultipleChoice choice) {
		return instructor.deleteMultipleChoiceEQ(choice.getModuleId(), choice.getQuestionId());
	}
	
	public boolean deleteNumerical(Numerical choice) {
		return instructor.deleteNumericalEQ(choice.getModuleId(), choice.getQuestionId());
	}

	public boolean deleteShortBlank(ShortBlank choice) {
		return instructor.deleteShortBlankEQ(choice.getModuleId(), choice.getQuestionId());
	}

	public boolean deleteModule(long moduleId) {
		if(!instructor.deleteModule(moduleId))
			return false;
		
/*		if(!instructor.deleteModuleMultipleChoiceEQ(moduleId))			
			return false;
		
		if(!instructor.deleteModuleNumericalEQ(moduleId))			
			return false;

		if(!instructor.deleteModuleShortBlankEQ(moduleId))			
			return false;*/
		
		if(!instructor.deleteModuleEQs(moduleId))
			return false;

		return true;
	}
	
	public boolean deleteQuestion(Question question) {
		
		if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.MC))) {
			return deleteMultipleChoice((MultipleChoice) question);
		} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.NUM))) {
			return deleteNumerical((Numerical) question);
		} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.SHORT))) {
			return deleteShortBlank((ShortBlank) question);
		}
		
		return false;
	}
}
