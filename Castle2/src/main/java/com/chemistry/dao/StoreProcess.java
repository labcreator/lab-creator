package com.chemistry.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.chemistry.model.Assignment;

@Repository
@Scope("session")
	public class StoreProcess {
		
	public static final String driver = "com.mysql.jdbc.Driver";
	public static final String url2 = "jdbc:mysql://localhost/chproject";
	public static final String url1 = "jdbc:mysql://169.226.48.166/chemproject";

	public static final String user = "root";
	public static final String password = "051633b$";

	public static Connection connection = null;
		public Statement statement = null;
		public ResultSet resultSet = null;
		public PreparedStatement preparedStatement = null;
		
		static {
			try	{
				Class.forName(driver);
				System.out.println("Driver Loaded");
				connection = DriverManager.getConnection(url1, user, password);
				System.out.println("Connection ESTD");
			}
			catch(ClassNotFoundException e)	{
				e.printStackTrace();
			}
			catch(SQLException e){
				e.getMessage();
				try {
					connection = DriverManager.getConnection(url2, user, password);
					System.out.println("Connection ESTD");

				} catch (SQLException e1) {
					e1.printStackTrace();
				}				
			}
		}
		
		//************ Database Connections ***************
		public ResultSet getResultSetQuery(String sql) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sql);
			return ps.executeQuery();
		}
		
		public int insertSQLQuery(String sql) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sql);
			return ps.executeUpdate();
		}
		
		public int updateSQLQuery(String sql) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sql);
			return ps.executeUpdate();
		}

		public int deleteSQLQuery(String sql) throws SQLException {
			return updateSQLQuery(sql);
		}
		
		public List<Assignment> getExperimentList() {
			
			String sql = "select * from experiment";
			
			List<Assignment> list = new ArrayList<Assignment>();
			try {
				ResultSet rs = getResultSetQuery(sql);
				while(rs.next()) {
					Assignment assignment = new Assignment();
					assignment.setExperimentId(rs.getInt("id"));
					assignment.setExperimentName(rs.getString("name"));
					list.add(assignment);
				}			
				return list;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
}
