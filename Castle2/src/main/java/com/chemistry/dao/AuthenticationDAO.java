package com.chemistry.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.Registration;
import com.chemistry.model.authenticate.User;

@Repository
public class AuthenticationDAO extends StoreProcess {


	public boolean isUserExists(Login login) {
		
		String sql = "select * from login where netid = '"+login.getNetId()+ "' and "
											 + "firstname = '"+login.getFirstName()+"' and "
											 + "usertype = '"+login.getUserType()+"'";
		System.out.println(sql);
		try {
			ResultSet rs = getResultSetQuery(sql);
		//	System.out.println("Result Set : "+rs.next());
			if(rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean insertRegistrationDetails(Registration register) {
		
		String sql = "insert into login values(?,?,?,?,?)";
		
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, register.getNetId());
			ps.setString(2, register.getFirstName());
			ps.setString(3, register.getLastName());
			ps.setString(4, register.getUserType());
			ps.setTimestamp(5, register.getSQLTimeStamp());
			return ps.executeUpdate() > 0;			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean createUserAssignment(String netId) {
		String sql = "insert into userassignment(netid) values('"+netId+"')";
		
		try {
			return insertSQLQuery(sql) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public User getUserAssignment(String netId) {
		String sql = "select * from userassignment where netid = '"+netId+"'";
		User user = new User();
		try {
			ResultSet rs =  getResultSetQuery(sql);
			if(rs.next()) {
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean deleteNewUser(String netId) {
		String sql = "delete from login where netid = '"+netId+"'";
		try {
			return deleteSQLQuery(sql) > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
}
