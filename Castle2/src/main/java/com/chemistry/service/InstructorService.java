package com.chemistry.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.dao.users.InstructorDAO;
import com.chemistry.enums.QuestionMember;
import com.chemistry.enums.QuestionType;
import com.chemistry.model.Experiment;
import com.chemistry.model.Module;
import com.chemistry.model.questions.DataEntry;
import com.chemistry.model.questions.ExperimentQuestions;
import com.chemistry.model.questions.MultipleChoice;
import com.chemistry.model.questions.Numerical;
import com.chemistry.model.questions.Question;
import com.chemistry.model.questions.ShortBlank;

@Service
@Scope("session")
public class InstructorService {

	private InstructorDAO instructor = null;
	private QuestionMember member = new QuestionMember();
	final int NOT_INSERTED = 0;
	
	@Autowired
	public void setInstructor(InstructorDAO instructor) {
		this.instructor = instructor;
	}

	// ************ Group Questions ************
	public boolean generateExperiment(Experiment experiment) {
		try {
			int id = createExperiment(experiment.getName());
			createModules(experiment.getModules());
			createPrelab(experiment.getPrelab(), id);	
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public int createExperiment(String name) {
		return instructor.createExperiment(name);
	}
	
	public boolean createModules(Map<Long, Module> modules)  {
	
		Module[] m = new Module[modules.size()];
		int i = 0;
		for(Map.Entry<Long, Module> map : modules.entrySet()) {
			m[i] = map.getValue(); 
			i++;
		}
		
		return instructor.createModules(m);
	}
	
	public boolean createPrelab(Map<Long, Question> prelab, int id) {
		
		List<ExperimentQuestions> experiment = new ArrayList<ExperimentQuestions>();
		List<MultipleChoice> choice = new ArrayList<MultipleChoice>();
		List<Numerical> numerical = new ArrayList<Numerical>();
		List<ShortBlank> blank = new ArrayList<ShortBlank>();

		try {
			for(Map.Entry<Long, Question> map : prelab.entrySet()) {
				Question question = map.getValue();
				if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.MC))) {
					experiment.add(makeMultipleChoice((MultipleChoice) question, id));
					choice.add((MultipleChoice) question);
				} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.NUM))) {
					experiment.add(makeNumerical((Numerical) question, id));
					numerical.add((Numerical) question);				
				} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.SHORT))) {
					experiment.add(makeShortBlank((ShortBlank) question, id));
					blank.add((ShortBlank) question);				
				}
			}
			
			createMultipleChoice(choice);
			createNumerical(numerical);
			createShortBlank(blank);
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean createExperimentalQuestions(List<ExperimentQuestions> experiment) {
		return instructor.createExperimentalQuestions(experiment);
	}
	
	public boolean createMultipleChoice(List<MultipleChoice> choice) {
		return instructor.createMultipleChoice(choice);
	}
	
	public boolean createNumerical(List<Numerical> numerical) {
		return instructor.createNumerical(numerical);
	}
	
	public boolean createShortBlank(List<ShortBlank> blank) {
		return instructor.createShortBlank(blank);
	}
	
	private ExperimentQuestions makeDataEntry(DataEntry question, int id) {
		ExperimentQuestions eQuestion = new ExperimentQuestions();
		eQuestion.setExperimentId(id);
		eQuestion.setModuleId(question.getModuleId());
		eQuestion.setMcId(NOT_INSERTED);
		eQuestion.setNumId(NOT_INSERTED);
		eQuestion.setShortId(NOT_INSERTED);
		eQuestion.setDeId(question.getQuestionId());
		return eQuestion;
	}
	
	private ExperimentQuestions makeMultipleChoice(MultipleChoice question, int id) {
		ExperimentQuestions eQuestion = new ExperimentQuestions();
		eQuestion.setExperimentId(id);
		eQuestion.setModuleId(question.getModuleId());
		eQuestion.setMcId(question.getQuestionId());
		eQuestion.setNumId(NOT_INSERTED);
		eQuestion.setShortId(NOT_INSERTED);
		eQuestion.setDeId(NOT_INSERTED);
		return eQuestion;
	}
	
	private ExperimentQuestions makeNumerical(Numerical question, int id) {
		ExperimentQuestions eQuestion = new ExperimentQuestions();
		eQuestion.setExperimentId(id);
		eQuestion.setModuleId(question.getModuleId());
		eQuestion.setMcId(NOT_INSERTED);
		eQuestion.setNumId(question.getQuestionId());
		eQuestion.setShortId(NOT_INSERTED);
		eQuestion.setDeId(NOT_INSERTED);
		return eQuestion;
	}
	
	private ExperimentQuestions makeShortBlank(ShortBlank question, int id) {
		ExperimentQuestions eQuestion = new ExperimentQuestions();
		eQuestion.setExperimentId(id);
		eQuestion.setModuleId(question.getModuleId());
		eQuestion.setMcId(NOT_INSERTED);
		eQuestion.setNumId(NOT_INSERTED);
		eQuestion.setShortId(question.getQuestionId());
		eQuestion.setDeId(NOT_INSERTED);
		return eQuestion;		
	}
	
	// *********** Single Entries **************
	public boolean createModule(Module module) {
		return instructor.createModule(module);
	}
	
	public boolean updateModule(Module module) {
		return instructor.updateModule(module);
	}
	
	public boolean deleteModule(long id) {
		return instructor.deleteModule(id);
	}
	
	public boolean deleteQuestion(Question question) {
		return instructor.deleteQuestion(question);
	}
	
	public boolean addQuestion(Question question, int id) {
		try {
		ExperimentQuestions experiment = null;
		if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.MC))) {
			experiment = makeMultipleChoice((MultipleChoice) question, id);
			instructor.addMultipleChoice((MultipleChoice) question);
		} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.NUM))) {
			experiment = makeNumerical((Numerical) question, id);
			instructor.addNumerical((Numerical) question);				
		} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.SHORT))) {
			experiment = makeShortBlank((ShortBlank) question, id);
			instructor.addShortBlank( (ShortBlank)question);				
		} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.DATA))) {
			experiment = makeDataEntry((DataEntry) question, id);
			instructor.addDataEntry( (DataEntry)question);				
		}
		
		instructor.addExperimentQuestions(experiment);
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean editQuestion(Question question, int id) {
		try {
			ExperimentQuestions experiment = null;
			if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.MC))) {
				instructor.editMultipleChoice((MultipleChoice) question);
			} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.NUM))) {
				instructor.editNumerical((Numerical) question);				
			} else if(question.getQuestionType().equalsIgnoreCase(member.type(QuestionType.SHORT))) {
				instructor.editShortBlank( (ShortBlank)question);				
			}
		
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
