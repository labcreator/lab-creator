package com.chemistry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.dao.AuthenticationDAO;
import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.Registration;
import com.chemistry.model.authenticate.User;

@Service
@Scope("session")
public class AuthenticationService {
	
	private AuthenticationDAO authenticate = null;
	
	@Autowired
	public void setAuthenticate(AuthenticationDAO authenticate) {
		this.authenticate = authenticate;
	}

	public boolean createNewUser(Registration registration) {
		if(authenticate.insertRegistrationDetails(registration))
			return authenticate.createUserAssignment(registration.getNetId());
		
		return false;
	}
	
	public boolean deleteNewUser(Registration registration) {
		return authenticate.deleteNewUser(registration.getNetId());
	}
	
	public User isUserExists(Login login) {
		User user = null;
		boolean t = authenticate.isUserExists(login);
		System.out.println("T : "+t);
		if(t)
			user = authenticate.getUserAssignment(login.getNetId());		
		return user;
	}
	
}
