package com.chemistry.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.dao.users.StudentDAO;
import com.chemistry.model.Assignment;
import com.chemistry.model.Module;
import com.chemistry.model.SelectExperiment;
import com.chemistry.model.questions.Question;

@Service
@Scope("session")
public class StudentService  {

	private StudentDAO student = null;
	private SelectExperiment selectExperiment = null;
	private int experimentId = 0;
	
	private Map<Module, List<Question>> prelab = null;
	private Map<Module, List<Question>> lab = null;
	private Map<Module, List<Question>> postlab = null;

	private Map<String, List<Question>> prelabModName = null;
	private Map<String, List<Question>> labmodName = null;
	private Map<String, List<Question>> postlabModName = null;

	
	private List<Module> modules = null;
	
	@Autowired
	public void setStudent(StudentDAO student) {
		this.student = student;
	}
	
	public List<Assignment> getExperimentList() {
		return student.getExperimentList();
	}
	
	public int getExperimentId() {
		return this.experimentId;
	}
	
	public SelectExperiment getSelectedExperiment() {
		return selectExperiment;
	}
	
	public Map<Module, List<Question>> getPrelab() {
		return prelab;
	}

	public void setPrelab(Map<Module, List<Question>> prelab) {
		this.prelab = prelab;
	}

	public Map<Module, List<Question>> getLab() {
		return lab;
	}

	public void setLab(Map<Module, List<Question>> lab) {
		this.lab = lab;
	}

	public Map<Module, List<Question>> getPostlab() {
		return postlab;
	}

	public void setPostlab(Map<Module, List<Question>> postlab) {
		this.postlab = postlab;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Module> getModules() {
		return modules;
	}
	
	public SelectExperiment getExperiment(int id) {
		this.experimentId = id;
		this.selectExperiment = student.getSelectedExperiment(id);
		this.modules = student.getModules();
		this.prelab = student.getPrelab();
		this.lab = student.getLab();
		this.postlab = student.getPostlab();
		
		this.prelabModName = student.getPrelabModName();
		this.labmodName = student.getLabModName();
		this.postlabModName = student.getPostlabModName();
		
		return this.selectExperiment;
	}
	
	public List<Question> getPrelabQuestions(Module module) {
		return this.prelab.get(module);
	}
	
	public List<Question> getPrelabQuestionsModuleID(long moduleid) {
		
		Module module = moduleIterator(moduleid);		
		if(module != null)
			this.prelab.get(module);
		
		return null;
	}

	public List<Question> getLabQuestions(Module module) {
		return this.lab.get(module);
	}

	public List<Question> getLabQuestionsModuleID(long moduleid) {
		
		Module module = moduleIterator(moduleid);		
		if(module != null)
			this.lab.get(module);
		
		return null;
	}

	public List<Question> getPostlabQuestions(Module module) {
		return this.postlab.get(module);
	}

	public List<Question> getPostlabQuestionsModuleID(long moduleid) {
		
		Module module = moduleIterator(moduleid);		
		if(module != null)
			this.postlab.get(module);
		
		return null;
	}
	
	private Module moduleIterator(long moduleid) {

		Iterator<Module> iterate = modules.iterator();
		while(iterate.hasNext()) {
			Module module = iterate.next();
			if(module.getId() == moduleid) {
				return module;
			}
		}
		
		return null;
	}
}
