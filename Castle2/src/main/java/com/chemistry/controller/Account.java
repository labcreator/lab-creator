package com.chemistry.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.chemistry.json.instructor.AuthenticationJSON;
import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.Registration;
import com.chemistry.model.authenticate.User;
import com.chemistry.service.AuthenticationService;

@Controller
@Scope("session")
public class Account {

	int NETID_LENGTH = 5;
	static User user = null;
	static Login mainLogin = null;
	
	private AuthenticationService authenticateService = null;
	
	@Autowired
	public void setAuthenticateService(AuthenticationService authenticateService) {
		this.authenticateService = authenticateService;
	}
	
	@RequestMapping(value="register.html")
	public String handleRegister() {
		return "register";
	}
	@RequestMapping(value="login.html")
	public String handleLogin() {
		return "login";
	}
	@RequestMapping(value="register_post.html", method=RequestMethod.POST)
	public String handleRegistration( HttpServletRequest request, ModelMap map) {
		Registration register = new Registration();
		register.setNetId(request.getParameter("netid"));
		register.setFirstName(request.getParameter("firstname"));
		register.setLastName(request.getParameter("lastname"));

		Login login = new Login(register.getNetId(), register.getFirstName());
		user = authenticateService.isUserExists(login);
		
		if(register.getNetId().length() < NETID_LENGTH) {
			map.addAttribute("status", "Check your NetID or FirstName");
			return "register";
		}
		else if(user != null) 
			map.addAttribute("status", "Account Already Exists.");
		else if(authenticateService.createNewUser(register))
			map.addAttribute("status", "Account Created Successfully..");
		else {
			authenticateService.deleteNewUser(register);
			map.addAttribute("status", "Account Not created. Try Again!");
		}
		return "login";
	}
		
	@RequestMapping(value="authenticate.html", method=RequestMethod.POST)
	public String userAuthentication(HttpServletRequest request, ModelMap map) {
		
		Login login = new Login();
		login.setNetId(request.getParameter("netid"));
		login.setFirstName(request.getParameter("firstname"));
		login.setUserType(request.getParameter("usertype"));
		
		System.out.println("In Authentication Page : "+login.getNetId());
		
		if(login.getNetId().length() < NETID_LENGTH) {
			map.addAttribute("status", "Check your NetID or FirstName");
			return "login";
		}

		User user = authenticateService.isUserExists(login);
		System.out.println("In Authentication User : "+user);

		mainLogin = login;
		
		if(user != null) {
			map.addAttribute("username", login.getCapitalFirstName());
			
			System.out.println("Came to Not null");
			if(login.getUserType().equalsIgnoreCase("instructor")) {
//				return "iwelcome";
				return "instructor/createassignment";
			} else if(login.getUserType().equalsIgnoreCase("student")) {
	//			return "swelcome";
				map.addAttribute("message", "First Message JSP");
				return "showMessage";
			} else {
				return "tawelcome";
			}
			
			/*		this.setUser(user);
			setExperiment(getStudent().getParticularExperiment(1));

		
			session = request.getSession();
			session.setAttribute("login", "true");
			session.setAttribute("user", getUser());*/
//			return "status";
		}
		else {
			map.addAttribute("status", "Login Failed");
			return "login";
		}
	}
}
