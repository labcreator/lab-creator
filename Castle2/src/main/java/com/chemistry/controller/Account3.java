package com.chemistry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chemistry.json.instructor.AuthenticationJSON;
import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.Registration;
import com.chemistry.model.authenticate.User;
import com.chemistry.service.AuthenticationService;
import com.chemistry.userfactory.ChemistryFactory;
import com.chemistry.userfactory.UserEnumFactory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
@Scope("session")
public class Account3 {

	final int NETID_LENGTH = 5;
	private AuthenticationJSON authenticate = null;
	private AuthenticationService authenticateService = null;
	/*private UserEnumFactory userFactory = null;
	
	@Autowired
	public void setUserFactory(UserEnumFactory userFactory) {
		this.userFactory = userFactory;
	}*/

	@Autowired
	public void setAuthenticationJSON(AuthenticationJSON authenticate) {
		this.authenticate = authenticate;
	}
	
	@Autowired
	public void setAuthenticateService(AuthenticationService authenticateService) {
		this.authenticateService = authenticateService;
	}

	@RequestMapping(value="login")
	public String loginPage() {
		System.out.println("Login Page");
		return "login";
	}
	
	@RequestMapping(value="login.json", method=RequestMethod.POST)
	public @ResponseBody String loginValidation(@RequestParam String login) {
		
		System.out.println("In Login JSON");
		JsonParser parse = new JsonParser();
		JsonElement element = parse.parse(login);
		JsonObject object = element.getAsJsonObject();
		Login loginD = authenticate.loginJSON(object);
		
		validateLogin(loginD);
		
		return "login";
	}
	
	public @ResponseBody String validateLogin(Login login) {
		String status = "";
		String url = "welcome";
		ModelMap map = new ModelMap();
		
		if(login.getNetId().length() < NETID_LENGTH) {
			status = "NetID length must be greater than 5 characters";
			url = "login";
		}
		
		User user = authenticateService.isUserExists(login);
		if(user == null){
			status = "Account Not Exists";
		} else {	
	//		ChemistryFactory factory = userFactory.userFactory(login.getUserTypeEnum());
			
			if(login.getUserType().equalsIgnoreCase("instructor")) {
				url = "iwelcome";
			} else if(login.getUserType().equalsIgnoreCase("student")) {
				url = "swelcome";
			} else {
				url = "tawelcome";
			}
			status = login.getFirstName();
		}
		
		map.addAttribute("status", status);
		return url;
	}
	
	@RequestMapping(value="register.html", method=RequestMethod.GET)
	public String registerPage() {
		return "register";
	}
	
	@RequestMapping(value="register.json", method=RequestMethod.POST)
	public void registerPage(@RequestParam String register) {
		System.out.println("In Register Page");
		
		JsonParser parse = new JsonParser();
		JsonElement element = parse.parse(register);
		JsonObject object = element.getAsJsonObject();
		
		Registration registration = authenticate.registerJSON(object);
	validateRegistration(registration);
		
/*		String gson = new Gson().toJson(s);
		return gson;*/
	}
	
	public  ModelAndView validateRegistration(Registration registration) {
		String status = "";
		String url = "login";
		ModelMap map = new ModelMap();
		
		if(registration.getNetId().length() < NETID_LENGTH) {
			status = "NetID length must be greater than 5 characters";
			url = "register";
		}
		
		Login login = new Login(registration.getNetId(), registration.getFirstName(), registration.getUserType());
		User user = authenticateService.isUserExists(login);
		if(user == null){
			if(authenticateService.createNewUser(registration))
				status = "Account Created, Successfully";
		} else {	
			status = "Account Already Exists!";
		}
		
		System.out.println("status : "+status);
		map.addAttribute("status", status);
		System.out.println("URL : "+url);
		
		ModelAndView view = new ModelAndView(url);
		view.addObject("status", status);
		
		
		return view;
	}
	
	/*@RequestMapping(value="/call", method=RequestMethod.GET)
	public String requestShowMessage(ModelMap map) {
		map.addAttribute("message", "First Message JSP");
		return "showMessage";
	}
	*/
	
}
