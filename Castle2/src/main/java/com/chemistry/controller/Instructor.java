package com.chemistry.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chemistry.json.instructor.JsonAssignment;
import com.chemistry.model.Experiment;
import com.chemistry.model.Module;
import com.chemistry.model.questions.Question;
import com.chemistry.service.InstructorService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

@Controller
@Scope("session")
public class Instructor {

	private InstructorService instructor = null;
	private JsonAssignment jsonQuestion = null;
	final int ID = 1;

	@Autowired
	public void setInstructor(InstructorService instructor) {
		this.instructor = instructor;
	}
	
	@Autowired
	public void setJsonQuestion(JsonAssignment jsonQuestion) {
		this.jsonQuestion = jsonQuestion;
	}


	//	HrppSession session = 
	@RequestMapping(value="/iwelcome") 
	public String instructorWelcome() {
	//	System.out.println("Instructor User : "+user);
		return "iwelcome";
	}
	
	@RequestMapping(value="/CreateAssignment", method=RequestMethod.GET)
	public String requestCreateAssignment(ModelMap map) {
	//	System.out.println("Instructor User : "+user);
		return "instructor/createassignment";
	}

	
	@RequestMapping(value="createexperiment.json", method=RequestMethod.POST)
	public @ResponseBody String createExperiment(@RequestParam String experiment) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(experiment);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Experiment : ");
		System.out.println(object);

		String experimentName = object.get("assignmentName").getAsString();
		
		Experiment exper = new Experiment();
		exper.setName(experimentName);
		exper.setModules(jsonQuestion.getModules());
		exper.setPrelab(jsonQuestion.getPrelab());
		
		exper.iterateModules();
		exper.iteratePrelab();
		
		System.out.println("Generate : "+instructor.generateExperiment(exper));
		
		return " Experiemnt Added";
	}

	@RequestMapping(value="editmodule.json", method=RequestMethod.POST)
	public @ResponseBody String editModule(@RequestParam String module) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		try {
			System.out.println("Edit Module : ");
			System.out.println(object);
			
			Module mod = jsonQuestion.jsonEditModule(object);
			System.out.println("Converted");
	
			System.out.println("Edited Module : "+instructor.updateModule(mod));
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return " Module Edited";
	}
	
	@RequestMapping(value="addmodule.json", method=RequestMethod.POST)
	public @ResponseBody String addModule(@RequestParam String module) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Module : ");
		System.out.println(object);
		
		Module mod = jsonQuestion.jsonModule(object);
		System.out.println("Converted");
		
		System.out.println("Module Inserted : "+instructor.createModule(mod));
		return " Module Added";
	}
	
	@RequestMapping(value="deletemodule.json", method=RequestMethod.POST)
	public @ResponseBody String deleteModule(@RequestParam String module) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		Module mod = jsonQuestion.jsonModule(object);
		boolean b  = instructor.deleteModule(mod.getId());
		System.out.println("Delete Module : "+b);
		
		return "Deleted Module";
	}
	
	@RequestMapping(value="addquestion.json", method=RequestMethod.POST)
	public @ResponseBody String addQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Question : ");
		System.out.println(object);
		Question quest = jsonQuestion.jsonQuestion(object);
		instructor.addQuestion(quest, ID);
		
		//new JsonAssignment().jsonQuestion(object);
		System.out.println("Converted");
		
		return "Question Added";
	}
	
	@RequestMapping(value="editquestion.json", method=RequestMethod.POST)
	public @ResponseBody String editQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Edit Question : ");
		System.out.println(object);
		Question quest = jsonQuestion.jsonEditQuestion(object);
		
		//new JsonAssignment().jsonQuestion(object);
		boolean b = instructor.editQuestion(quest, ID);
		System.out.println("Edited "+b);
		
		if(b)
			return b+"";
		else 
			return b+"";
	}

	
	@RequestMapping(value="deletequestion.json", method=RequestMethod.POST)
	public @ResponseBody String deleteQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		Question quest = jsonQuestion.jsonEditQuestion(object);
		boolean b = instructor.deleteQuestion(quest);
		System.out.println("Delete Ques : "+b);
		
		return "Deleted question";
	}
	
	@RequestMapping(value="/createassignment.json", method=RequestMethod.GET)
	public @ResponseBody String requestAssignment(@RequestParam String assignment) {
		System.out.println("Came to Assignment JSON");
		System.out.println(assignment);
		
		try {
		JsonParser parser = new JsonParser();
		System.out.println("After {arser");
		JsonElement element = parser.parse(assignment);
		System.out.println("After Element");
		JsonObject trade = element.getAsJsonObject();
		JsonAssignment jAssignment = new JsonAssignment();

		System.out.println("Before Primitive");
		//jAssignment.jsonToAssignment(trade);

		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return "Data received";
	}

	@RequestMapping(value="/createquestions.json", method=RequestMethod.POST)
	public @ResponseBody String requestAssignmentQuestions(@RequestParam String questions) {
	
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(questions);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Questions : ");
		System.out.println(object);
		
		return "Questions Transferred";
	}

}
