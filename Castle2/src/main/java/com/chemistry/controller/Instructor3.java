package com.chemistry.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chemistry.json.instructor.JsonAssignment;
import com.chemistry.model.Experiment;
import com.chemistry.model.Module;
import com.chemistry.model.SelectExperiment;
import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.User;
import com.chemistry.model.questions.Question;
import com.chemistry.service.InstructorService;
import com.chemistry.service.StudentService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//@Controller
//@Scope("session")
public class Instructor3 {

	private StudentService student = null;
	private SelectExperiment experiment = null;
	private InstructorService instructor = null;
	private JsonAssignment jsonQuestion = null;
	final int ID = 1;

	@Autowired
	public void setInstructor(InstructorService instructor) {
		this.instructor = instructor;
	}
	
	@Autowired
	public void setJsonQuestion(JsonAssignment jsonQuestion) {
		this.jsonQuestion = jsonQuestion;
	}
	
	@Autowired
	public void setStudent(StudentService student) {
		this.student = student;
		this.experiment = student.getExperiment(1);
	}
	
	//@RequestMapping(value="swelcome")
	public String studentWelcomePage(ModelMap map) {
		User user = Account.user;
		Login login = Account.mainLogin;
		
		System.out.println("Login : "+login);
		System.out.println("User : "+user);
		return "swelcome";
	}
	
	@RequestMapping(value="experiments.json", method=RequestMethod.GET)
	public @ResponseBody String requestExperiments() {
	/*	Map<Module, List<Question>> prelab  = student.getPrelab();
		
		String json = new Gson().toJson(prelab);
		System.out.println("Prelab Json Request : ");
		System.out.println(json);
*/
		return "";
	}
	
	@RequestMapping(value="/call", method=RequestMethod.GET)
	public String requestShowMessage(ModelMap map) {
		map.addAttribute("message", "First Message JSP");
		this.experiment = student.getExperiment(1);
		
		User user = Account.user;
		Login login = Account.mainLogin;
		
		System.out.println("Login : "+login);
		System.out.println("User : "+user);
		
		return "showMessage";
	}
	
	
	@RequestMapping(value="prelab.json", method=RequestMethod.GET)
	public @ResponseBody String requestPrelabSection() {
		Map<Module, List<Question>> prelab  = student.getPrelab();
		
		String json = new Gson().toJson(prelab);
		System.out.println("Prelab Json Request 1 : ");
		System.out.println(json+"\n\n");

		System.out.println("Prelab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getPrelab()));
		
		return json;
	}
	
	@RequestMapping(value="lab.json", method=RequestMethod.GET)
	public @ResponseBody String requestLabSection() {
		Map<Module, List<Question>> lab  = student.getLab();
		
		String json = new Gson().toJson(lab);
		System.out.println("Lab Json Request 1 : ");
		System.out.println(json);

		System.out.println("Lab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getLab()));
		
		return json;
	}
	
	@RequestMapping(value="postlab.json", method=RequestMethod.GET)
	public @ResponseBody String requestPostlabSection() {
		Map<Module, List<Question>> postlab  = student.getPostlab();
		
		String json = new Gson().toJson(postlab);
		System.out.println("Postlab Json Request : ");
		System.out.println(json);

		System.out.println("Postlab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getPostlab()));

		return json;
	}
	



	//	HrppSession session = 
	@RequestMapping(value="/iwelcome") 
	public String instructorWelcome() {
	//	System.out.println("Instructor User : "+user);
		return "iwelcome";
	}
	
	@RequestMapping(value="/CreateAssignment", method=RequestMethod.GET)
	public String requestCreateAssignment(ModelMap map) {
	//	System.out.println("Instructor User : "+user);
		return "instructor/createassignment";
	}

	
	@RequestMapping(value="createexperiment.json", method=RequestMethod.POST)
	public @ResponseBody String createExperiment(@RequestParam String experiment) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(experiment);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Experiment : ");
		System.out.println(object);

		String experimentName = object.get("assignmentName").getAsString();
		
		Experiment exper = new Experiment();
		exper.setName(experimentName);
		exper.setModules(jsonQuestion.getModules());
		exper.setPrelab(jsonQuestion.getPrelab());
		
		exper.iterateModules();
		exper.iteratePrelab();
		
		System.out.println("Generate : "+instructor.generateExperiment(exper));
		
		return " Experiemnt Added";
	}

	@RequestMapping(value="editmodule.json", method=RequestMethod.POST)
	public @ResponseBody String editModule(@RequestParam String module) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		try {
			System.out.println("Edit Module : ");
			System.out.println(object);
			
			Module mod = jsonQuestion.jsonEditModule(object);
			System.out.println("Converted");
	
			System.out.println("Edited Module : "+instructor.updateModule(mod));
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return " Module Edited";
	}
	
	@RequestMapping(value="addmodule.json", method=RequestMethod.POST)
	public @ResponseBody String addModule(@RequestParam String module) {
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Module : ");
		System.out.println(object);
		
		Module mod = jsonQuestion.jsonModule(object);
		System.out.println("Converted");
		
		System.out.println("Module Inserted : "+instructor.createModule(mod));
		return " Module Added";
	}
	
	@RequestMapping(value="deletemodule.json", method=RequestMethod.POST)
	public @ResponseBody String deleteModule(@RequestParam String module) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(module);
		JsonObject object = element.getAsJsonObject();
		
		Module mod = jsonQuestion.jsonModule(object);
		boolean b  = instructor.deleteModule(mod.getId());
		System.out.println("Delete Module : "+b);
		
		return "Deleted Module";
	}
	
	@RequestMapping(value="addquestion.json", method=RequestMethod.POST)
	public @ResponseBody String addQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Add Question : ");
		System.out.println(object);
		Question quest = jsonQuestion.jsonQuestion(object);
		instructor.addQuestion(quest, ID);
		
		//new JsonAssignment().jsonQuestion(object);
		System.out.println("Converted");
		
		return "Question Added";
	}
	
	@RequestMapping(value="editquestion.json", method=RequestMethod.POST)
	public @ResponseBody String editQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Edit Question : ");
		System.out.println(object);
		Question quest = jsonQuestion.jsonEditQuestion(object);
		
		//new JsonAssignment().jsonQuestion(object);
		boolean b = instructor.editQuestion(quest, ID);
		System.out.println("Edited "+b);
		
		if(b)
			return b+"";
		else 
			return b+"";
	}

	
	@RequestMapping(value="deletequestion.json", method=RequestMethod.POST)
	public @ResponseBody String deleteQuestion(@RequestParam String question) {
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(question);
		JsonObject object = element.getAsJsonObject();
		
		Question quest = jsonQuestion.jsonEditQuestion(object);
		boolean b = instructor.deleteQuestion(quest);
		System.out.println("Delete Ques : "+b);
		
		return "Deleted question";
	}
	
	@RequestMapping(value="/createassignment.json", method=RequestMethod.GET)
	public @ResponseBody String requestAssignment(@RequestParam String assignment) {
		System.out.println("Came to Assignment JSON");
		System.out.println(assignment);
		
		try {
		JsonParser parser = new JsonParser();
		System.out.println("After {arser");
		JsonElement element = parser.parse(assignment);
		System.out.println("After Element");
		JsonObject trade = element.getAsJsonObject();
		JsonAssignment jAssignment = new JsonAssignment();

		System.out.println("Before Primitive");
		//jAssignment.jsonToAssignment(trade);

		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return "Data received";
	}

	@RequestMapping(value="/createquestions.json", method=RequestMethod.POST)
	public @ResponseBody String requestAssignmentQuestions(@RequestParam String questions) {
	
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(questions);
		JsonObject object = element.getAsJsonObject();
		
		System.out.println("Questions : ");
		System.out.println(object);
		
		return "Questions Transferred";
	}

}
