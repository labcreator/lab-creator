package com.chemistry.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chemistry.model.Module;
import com.chemistry.model.SelectExperiment;
import com.chemistry.model.authenticate.Login;
import com.chemistry.model.authenticate.User;
import com.chemistry.model.questions.Question;
import com.chemistry.service.StudentService;
import com.google.gson.Gson;

@Controller
@Scope("session")
public class Student {

	private StudentService student = null;
	private SelectExperiment experiment = null;
	
	@Autowired
	public void setStudent(StudentService student) {
		this.student = student;
		this.experiment = student.getExperiment(1);
	}
	
	//@RequestMapping(value="swelcome")
	public String studentWelcomePage(ModelMap map) {
		User user = Account.user;
		Login login = Account.mainLogin;
		
		System.out.println("Login : "+login);
		System.out.println("User : "+user);
		return "swelcome";
	}
	
	@RequestMapping(value="experiments.json", method=RequestMethod.GET)
	public @ResponseBody String requestExperiments() {
	/*	Map<Module, List<Question>> prelab  = student.getPrelab();
		
		String json = new Gson().toJson(prelab);
		System.out.println("Prelab Json Request : ");
		System.out.println(json);
*/
		return "";
	}
	
	@RequestMapping(value="/call", method=RequestMethod.GET)
	public String requestShowMessage(ModelMap map) {
		map.addAttribute("message", "First Message JSP");
		this.experiment = student.getExperiment(1);
		
		User user = Account.user;
		Login login = Account.mainLogin;
		
		System.out.println("Login : "+login);
		System.out.println("User : "+user);
		
		return "showMessage";
	}
	
	
	@RequestMapping(value="prelab.json", method=RequestMethod.GET)
	public @ResponseBody String requestPrelabSection() {
		Map<Module, List<Question>> prelab  = student.getPrelab();
		
		String json = new Gson().toJson(prelab);
		System.out.println("Prelab Json Request 1 : ");
		System.out.println(json+"\n\n");

		System.out.println("Prelab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getPrelabModName()));
		
		return json;
	}
	
	@RequestMapping(value="lab.json", method=RequestMethod.GET)
	public @ResponseBody String requestLabSection() {
		Map<Module, List<Question>> lab  = student.getLab();
		
		String json = new Gson().toJson(lab);
		System.out.println("Lab Json Request 1 : ");
		System.out.println(json);

		System.out.println("Lab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getLabModName()));
		
		return json;
	}
	
	@RequestMapping(value="postlab.json", method=RequestMethod.GET)
	public @ResponseBody String requestPostlabSection() {
		Map<Module, List<Question>> postlab  = student.getPostlab();
		
		String json = new Gson().toJson(postlab);
		System.out.println("Postlab Json Request : ");
		System.out.println(json);

		System.out.println("Postlab Json Request 2 : ");
		System.out.println(new Gson().toJson(this.experiment.getPostlabModName()));

		return json;
	}
}
