package com.chemistry.userfactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.service.InstructorService;

@Service
@Scope("session")
public class InstructorServiceFactory extends ChemistryFactory{
	private InstructorService instructor = null;

	@Autowired
	public void setInstructor(InstructorService instructor) {
		this.instructor = instructor;
	}

	public InstructorService getInstructor() {
		return instructor;
	}
	
}
