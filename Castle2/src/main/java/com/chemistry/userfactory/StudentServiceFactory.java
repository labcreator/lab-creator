package com.chemistry.userfactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.chemistry.service.StudentService;

@Service
@Scope("session")
public class StudentServiceFactory extends ChemistryFactory {

	private StudentService student = null;

	public StudentService getStudent() {
		return student;
	}

	@Autowired
	public void setStudent(StudentService student) {
		this.student = student;
	}
	
	
}
