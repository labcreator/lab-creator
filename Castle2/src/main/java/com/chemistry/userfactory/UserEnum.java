package com.chemistry.userfactory;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


public enum UserEnum {
	instructor, student, teachingassistant
}
