package com.chemistry.userfactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("session")
public class UserEnumFactory {

	static InstructorServiceFactory instructor = null;
	static StudentServiceFactory student = null;
	
	@Autowired
	public void setInstructor(InstructorServiceFactory instructor) {
		this.instructor = instructor;
	}

	@Autowired
	public void setStudent(StudentServiceFactory student) {
		this.student = student;
	}

	public static ChemistryFactory userFactory(UserEnum user) {
		
		switch(user) {
		case instructor : {
			return instructor;
		}
		case student : {
			return student;
		}
		case teachingassistant : {
		}
		}
		return null;
	}
}
