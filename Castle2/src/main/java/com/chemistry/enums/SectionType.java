package com.chemistry.enums;

public enum SectionType {

	PRE, LAB, POST
}
