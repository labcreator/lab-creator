package com.chemistry.enums;

public class QuestionMember {

	public static String type(QuestionType type) {
		switch(type) {
		case MC : {
//			return "CHECKBOX QUESTION";
			return "MC";
			
		}
		case NUM : {
//			return "BLANK QUESTION";
			return "NUM";
			
		}
		case SHORT : {
//			return "TEXT QUESTION";
			return "SHORT";
		}
		case DATA : {
//			return "TEXT QUESTION";
			return "DATA";
		}
		}
		return null;
	}
	
	public static String section(SectionType section) {
		switch(section) {
		case PRE: {
			return "pre";
		}
		case LAB:{
			return "lab";
		}
		case POST:{
			return "post";
		}
		}
		return null;
	}
	
	public static String instructorSection(SectionType section) {
		switch(section) {
		case PRE: {
			return "pre";
		}
		case LAB:{
			return "lab";
		}
		case POST:{
			return "post";
		}
		}
		return null;
	}

}
