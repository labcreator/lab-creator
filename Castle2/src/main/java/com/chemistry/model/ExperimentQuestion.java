package com.chemistry.model;

public interface ExperimentQuestion {
	
	void setQuestion(String question);
	String getQuestion();
	
	void setSection(String section);
	String getSection();
	
	void setQuestionType(String type);
	String getQuestionType();
	
	void setAnswer(String answer);
	String getAnswer();
}
