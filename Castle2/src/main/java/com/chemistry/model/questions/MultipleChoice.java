package com.chemistry.model.questions;

import java.util.HashSet;
import java.util.Set;

public class MultipleChoice extends Question {

	private String[] incorrect = new String[4];
	private String[] hint = new String[3];
	private Set<String> invalid = new HashSet<String>();

	public String[] getIncorrect() {
		return incorrect;
	}
	public String getIncorrect(int i) {
		return incorrect[i];
	}

	public void setIncorrect(String[] incorrect) {
		this.incorrect = incorrect;
	}
	public void setIncorrect(String incorrect, int i) {
		this.incorrect[i] = incorrect;
	}

	public String[] getHint() {
		return hint;
	}
	public String getHint(int i) {
		return hint[i];
	}

	public void setHint(String[] hint) {
		this.hint = hint;
	}
	public void setHint(String hint, int i) {
		this.hint[i] = hint;
	}
	public Set<String> getInvalid() {
		return invalid;
	}
	public void setInvalid(Set<String> invalid) {
		this.invalid = invalid;
	}
	
}
