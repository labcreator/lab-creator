package com.chemistry.model.questions;

import com.chemistry.model.ExperimentQuestion;

public class Question implements ExperimentQuestion {

	private String question;
	private String answer;
	private String section;
	private String questionType;
	private long questionId;
	private int questionNo;
	private long moduleId;
	
	
	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public int getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestion() {
		return question;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSection() {
		return section;
	}

	public void setQuestionType(String type) {
		this.questionType = type;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAnswer() {
		return answer;
	}

}
