package com.chemistry.model.questions;

public class ExperimentQuestions {

	private int id;
	private long experimentId;
	private long moduleId;
	private long mcId;
	private long numId;
	private long shortId;
	private long deId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getExperimentId() {
		return experimentId;
	}
	public void setExperimentId(long experimentId) {
		this.experimentId = experimentId;
	}
	public long getModuleId() {
		return moduleId;
	}
	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}
	public long getMcId() {
		return mcId;
	}
	public void setMcId(long mcId) {
		this.mcId = mcId;
	}
	public long getNumId() {
		return numId;
	}
	public void setNumId(long numId) {
		this.numId = numId;
	}
	public long getShortId() {
		return shortId;
	}
	public void setShortId(long shortId) {
		this.shortId = shortId;
	}
	public long getDeId() {
		return deId;
	}
	public void setDeId(long deId) {
		this.deId = deId;
	}
}
