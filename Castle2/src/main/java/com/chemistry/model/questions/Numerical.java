package com.chemistry.model.questions;

public class Numerical extends Question {

	double fromRange;
	double toRange;	
	private String[] hint = new String[3];

	public double getFromRange() {
		return fromRange;
	}
	public void setFromRange(double fromRange) {
		this.fromRange = fromRange;
	}
	public double getToRange() {
		return toRange;
	}
	public void setToRange(double toRange) {
		this.toRange = toRange;
	}
	public String[] getHints() {
		return hint;
	}
	public String getHints(int i) {
		return hint[i];
	}

	public void setHint(String[] hint) {
		this.hint = hint;
	}
	public void setHint(String hint, int i) {
		this.hint[i] = hint;
	}
}
