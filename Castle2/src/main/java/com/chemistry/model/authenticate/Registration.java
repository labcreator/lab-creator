package com.chemistry.model.authenticate;

public class Registration extends Login {

	private String lastName;	
	private java.util.Date today = new java.util.Date();
	
	public Registration() {	}

	public Registration(String netId, String firstName, String lastName) {
		super(netId, firstName);
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public java.sql.Date getSQLDate() {
		return new java.sql.Date(today.getTime());
	}
	
	public java.sql.Timestamp getSQLTimeStamp() {
		return new java.sql.Timestamp(today.getTime());
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserType() {
		super.setUserType("student");
		return super.getUserType();
	}
	
	@Override
	public String toString() {
		super.toString();
		return "Registration [lastName=" + lastName + "]";
	}
	
}
