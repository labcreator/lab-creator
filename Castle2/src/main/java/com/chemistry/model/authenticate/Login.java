package com.chemistry.model.authenticate;

import com.chemistry.userfactory.UserEnum;

public class Login {

	private String netId;
	private String firstName;
	private String userType;
	
	public Login() {}
	
	public Login(String netId, String firstName) {
		this.netId = netId;
		this.firstName = firstName;
	}
	
	public Login(String netId, String firstName, String userType) {
		this.netId = netId;
		this.firstName = firstName;
		this.userType = userType;
	}
	
	public String getNetId() {
		return netId.toUpperCase();
	}
	public void setNetId(String netId) {
		this.netId = netId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getCapitalFirstName() {
		return " "+firstName.substring(0, 1).toUpperCase()+firstName.substring(1);
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "Login [netId=" + netId + ", firstName=" + firstName
				+ ", userType=" + userType + "]";
	}
	
	public UserEnum getUserTypeEnum() {
		if(getUserType().equalsIgnoreCase("instructor"))
			return UserEnum.instructor;
		else if(getUserType().equalsIgnoreCase("student"))
			return UserEnum.student;
		else
			return UserEnum.teachingassistant;
	}
}
