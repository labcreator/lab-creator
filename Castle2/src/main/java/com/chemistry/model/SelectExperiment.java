package com.chemistry.model;

import java.util.List;
import java.util.Map;

import com.chemistry.model.questions.Question;

public class SelectExperiment {

	private String name;
	private List<Module> modules;
	private Map<Module, List<Question>> prelab = null;
	private Map<Module, List<Question>> lab = null;
	private Map<Module, List<Question>> postlab = null;

	private Map<String, List<Question>> prelabModName = null;
	private Map<String, List<Question>> labModName = null;
	private Map<String, List<Question>> postlabModName = null;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Module> getModules() {
		return modules;
	}
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	public Map<Module, List<Question>> getPrelab() {
		return prelab;
	}
	public void setPrelab(Map<Module, List<Question>> prelab) {
		this.prelab = prelab;
	}
	public Map<Module, List<Question>> getLab() {
		return lab;
	}
	public void setLab(Map<Module, List<Question>> lab) {
		this.lab = lab;
	}
	public Map<Module, List<Question>> getPostlab() {
		return postlab;
	}
	public void setPostlab(Map<Module, List<Question>> postlab) {
		this.postlab = postlab;
	}
	public Map<String, List<Question>> getPrelabModName() {
		return prelabModName;
	}
	public void setPrelabModName(Map<String, List<Question>> prelabModName) {
		this.prelabModName = prelabModName;
	}
	public Map<String, List<Question>> getLabModName() {
		return labModName;
	}
	public void setLabModName(Map<String, List<Question>> labModName) {
		this.labModName = labModName;
	}
	public Map<String, List<Question>> getPostlabModName() {
		return postlabModName;
	}
	public void setPostlabModName(Map<String, List<Question>> postlabModName) {
		this.postlabModName = postlabModName;
	}
	
}
