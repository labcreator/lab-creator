package com.chemistry.model.util;

import java.util.List;

import com.chemistry.model.questions.Question;

public class UtilitySection {

	private List<Question> prelab = null;
	private List<Question> lab = null;
	private List<Question>postlab = null;
	public List<Question> getPrelab() {
		return prelab;
	}
	public void setPrelab(List<Question> prelab) {
		this.prelab = prelab;
	}
	public List<Question> getLab() {
		return lab;
	}
	public void setLab(List<Question> lab) {
		this.lab = lab;
	}
	public List<Question> getPostlab() {
		return postlab;
	}
	public void setPostlab(List<Question> postlab) {
		this.postlab = postlab;
	}
	
	
}
