package com.chemistry.model;

public class Module {

	private long id;
	private String moduleName;
	private String fileName;
//	private File file;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
