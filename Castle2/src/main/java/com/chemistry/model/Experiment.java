package com.chemistry.model;

import java.util.Map;

import com.chemistry.model.questions.Question;

public class Experiment {

	private String name;
	private Map<Long, Module> modules;
	private Map<Long, Question> prelab;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<Long, Module> getModules() {
		return modules;
	}
	public void setModules(Map<Long, Module> modules) {
		this.modules = modules;
	}
	public Map<Long, Question> getPrelab() {
		return prelab;
	}
	public void setPrelab(Map<Long, Question> prelab) {
		this.prelab = prelab;
	}
	
	public void iterateModules() {
		for(Map.Entry<Long, Module> entry : modules.entrySet()) {
			System.out.println(" "+entry.getKey()+" "+entry.getValue().getModuleName());
		}
	}
	
	public void iteratePrelab() {
		for(Map.Entry<Long, Question> entry : prelab.entrySet()) {
			System.out.println("\n\nKey : "+entry.getKey());
			
			System.out.println("Question :  "+entry.getValue().getQuestion());
			System.out.println("Question No : "+entry.getValue().getQuestionNo());
			System.out.println("Module No : "+entry.getValue().getModuleId());
			System.out.println("Answer :  "+entry.getValue().getAnswer());
			System.out.println("Question Id :  "+entry.getValue().getQuestionId());
			System.out.println("Question Type :  "+entry.getValue().getQuestionType());
			System.out.println("Section :  "+entry.getValue().getSection());
		}
	}
}
