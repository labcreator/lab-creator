var modArray = []; ///array of modules
var filesArray = [];
var currentMod;  /////current selected module id
var currentSection;  //////current section pre/lab/post
var allQuestions = []; ////array of all questions created
var assignmentquestions = [];
var xmlhttp = new XMLHttpRequest();

window.onbeforeunload = function()
{
    console.log("UNLOAD TEST");
    var a = document.getElementById("aName").value;
    sessionStorage.setItem("assignment", JSON.stringify({assignment: a}));
    sessionStorage.setItem("modArray", JSON.stringify(modArray));
    sessionStorage.setItem("questionArray", JSON.stringify(allQuestions));
}

window.onload = function()
{
    var i;
    console.log("TEST");
    console.log(sessionStorage.getItem("modArray"));
    if(sessionStorage.getItem("assignment") != null)
        document.getElementById("aName").value = JSON.parse(sessionStorage.getItem("assignment")).assignment;
    if(sessionStorage.getItem("modArray") != null)
    {
        var objArray = JSON.parse(sessionStorage.getItem("modArray"));
        console.log(objArray);
        for(i = 0; i < objArray.length; i++)
        {
            var obj = objArray[i];
            modArray.push(obj);
            addModElement(obj);
        }
    }
    if(sessionStorage.getItem("questionArray") != null)
        allQuestions = JSON.parse(sessionStorage.getItem("questionArray"));
}   

function modCreate(){
    
    console.log(document.getElementById("moduleId").value);
    
    if(document.getElementById("moduleId").value == "")
    {
        var text = document.getElementById("name").value;    
        var f = document.getElementById("file");
        var id = Date.now();
    //    var encoded = window.btoa(f.files[0]);
     //   var mod = {id:id, question:text, fname:f.files[0].name, file:encoded};
        var mod = {id:id, question:text, fname:f.files[0].name};
        
        modArray.push(mod);
        //filesArray.push(fileObj);
        addModElement(mod);
        
        var url = "addmodule.json?module="+JSON.stringify(mod);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
        
        console.log("RETURNED");
    }
    else ///////////EDIT MODE////////////////
    {
        var id = document.getElementById("moduleId").value;
        var mod = getObj(id,modArray);
        
        mod.question = document.getElementById("name").value;
      //  mod.file = window.btoa(document.getElementById("file").files[0]);
     //   mod.fname = mod.file.name;
        mod.fname = document.getElementById("file").files[0].name;
        
        document.getElementById(id).innerHTML = "";
        
        var fname = document.createTextNode(" PDF: " + mod.fname);   
        var span = document.createElement("SPAN")
        span.style.color="green";
        document.getElementById(id).appendChild(document.createTextNode(mod.question + " "));
        span.appendChild(fname);
        document.getElementById(id).appendChild(span);
        
        console.log('Module : '+JSON.stringify(mod));
        
        var url = "editmodule.json?module="+JSON.stringify(mod);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
    }
    clearMod();
}
function clearMod()
{
    document.getElementById("moduleId").value = "";
    document.getElementById("name").value = "";
    document.getElementById("file").value = "";
}
function addModElement(obj)
{
    var fname = document.createTextNode(" PDF: " + obj.fname);
    var n =document.createTextNode(obj.question + " ");    
    var span = document.createElement("SPAN")
    span.style.color="green";

    var li = createLi(obj.id);
    li.setAttribute("data-toggle", "pill");
    li.setAttribute("onclick", "setCurMod("+obj.id+")");
    li.appendChild(n);
    span.appendChild(fname);
    li.appendChild(span);
    
    var button = createEditButton("#modModal", obj.id);
    var xbutton = createDelButton(obj.id);

    var element = document.getElementById("modUL");
    element.appendChild(xbutton);
    element.appendChild(li);
    element.appendChild(button);
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
}

function setCurMod(id)
{
    var i;
    currentMod = id;
    var names = document.getElementsByClassName("currentMod");
    var buttons = document.getElementsByClassName("qButton");
    var curModName = getObj(id, modArray).question;
    
    for( i = 0; i < names.length; i++)
    {
        names[i].innerHTML = curModName;
        buttons[i].setAttribute("style", "display:block");
    }
    //////clear question lists first//////
    loadModQuestions(id);
    ////load questions////
}

function getObj(cid, array)
{
    var i;
    for(i = 0; i < array.length; i++)
    {
        if(array[i].id == cid)
            return array[i];
    }
    return "";
}

function loadModQuestions(modId) 
{
    var i;
    document.getElementById("assessUL").innerHTML = "";
    document.getElementById("labUL").innerHTML = "";
    document.getElementById("postUL").innerHTML = "";
    for(i = 0; i < allQuestions.length; i++)
    {
        if(allQuestions[i].modId == modId)
        {
            if(allQuestions[i].type == "mc")
                addMCElement(allQuestions[i]);
            else if(allQuestions[i].type == "num")
                addNumElement(allQuestions[i]);
            else if(allQuestions[i].type == "short")
                addShortElement(allQuestions[i]);
            else
                addDataElement(allQuestions[i]);
        }
    }
}

function setSection(sec)
{
    currentSection = sec;
}

function mcqCreate()
{
    if(document.getElementById("mcId").value == "")
    {
        console.log('In IF mcqcreate');
        var qname = document.getElementById("mcname").value;
        var id = Date.now();
        var question = {modId:currentMod, id:id, section:currentSection, type:"mc", question:qname, cAns:document.getElementById("mcAns").value, i1:document.getElementById("mciAns1").value, i2:document.getElementById("mciAns2").value, i3:document.getElementById("mciAns3").value, h1:document.getElementById("mchint1").value, h2:document.getElementById("mchint2").value, h3:document.getElementById("mchint3").value};

        allQuestions.push(question);
        addMCElement(question);
        
        console.log('Question : '+JSON.stringify(question));
        
        var url = "addquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
        
    }
    else
    {
        console.log('In ELSE mcqcreate');

        var id = document.getElementById("mcId").value;
        var question = getObj(id, allQuestions);
        
        question.question = document.getElementById("mcname").value;
        question.cAns = document.getElementById("mcAns").value;            
        question.i1 = document.getElementById("mciAns1").value;
        question.i2 = document.getElementById("mciAns2").value;
        question.i3 = document.getElementById("mciAns3").value;
        question.h1 = document.getElementById("mchint1").value;
        question.h2 = document.getElementById("mchint2").value;
        question.h3 = document.getElementById("mchint3").value;
        
       /* document.getElementById(id).innerHTML = "";
        document.getElementById(id).appendChild(document.createTextNode(question.question));
        */
        console.log('Question : '+JSON.stringify(question));
        
        var url = "editquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
                if(xmlhttp.responseText === 'true') {
                	console.log('Question Edited');
                	document.getElementById(id).innerHTML = "";
                	document.getElementById(id).appendChild(document.createTextNode(question.question));
                } else {
                	console.log('Question Not Edited');
                }
            }
        }
        xmlhttp.send();
    }
    clearMC();
}

function clearMC()
{
    document.getElementById("mcId").value = "";
    document.getElementById("mcname").value = "";
    document.getElementById("mcAns").value = "";
    document.getElementById("mciAns1").value = "";
    document.getElementById("mciAns2").value = "";
    document.getElementById("mciAns3").value = "";
    document.getElementById("mchint1").value = "";
    document.getElementById("mchint2").value = "";
    document.getElementById("mchint3").value = "";
}

function addMCElement(obj)
{
    var n =document.createTextNode(obj.question);                
    var li = createLi(obj.id);
    li.appendChild(n);

    var button = createEditButton("#mcModal", obj.id);
    var xbutton = createDelButton(obj.id);

    var element;
    if(obj.section == "pre")
        element = document.getElementById("assessUL");
    else if(obj.section == "lab")
        element = document.getElementById("labUL");
    element.appendChild(xbutton);
    element.appendChild(li);
    element.appendChild(button);
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    console.log(allQuestions);
}

function numCreate()
{
    if(document.getElementById("numId").value == "")
    {
        var qname = document.getElementById("numname").value;
        var id = Date.now();
        var question = {modId:currentMod, id:id, section:currentSection, type:"num", question:qname, cAns:document.getElementById("numcAns").value, r1:document.getElementById("numrange1").value, r2:document.getElementById("numrange2").value, h1:document.getElementById("numhint1").value, h2:document.getElementById("numhint2").value, h3:document.getElementById("numhint3").value};

        allQuestions.push(question);
        addNumElement(question);
        
        console.log('Question : '+JSON.stringify(question));
        
        var url = "addquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
    }
    else
    {
        var id = document.getElementById("numId").value;
        var question = getObj(id, allQuestions);
        
        question.question = document.getElementById("numname").value;
        question.cAns = document.getElementById("numcAns").value;            
        question.r1 = document.getElementById("numrange1").value;
        question.r2 = document.getElementById("numrange2").value;
        question.h1 = document.getElementById("numhint1").value;
        question.h2 = document.getElementById("numhint2").value;
        question.h3 = document.getElementById("numhint3").value;
        
      /*  document.getElementById(id).innerHTML = "";
        document.getElementById(id).appendChild(document.createTextNode(question.question));*/
        
     var url = "editquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
                if(xmlhttp.responseText === 'true') {
                	console.log('Question Edited');
                	document.getElementById(id).innerHTML = "";
                	document.getElementById(id).appendChild(document.createTextNode(question.question));
                } else {
                	console.log('Question Not Edited');
                }
            }
        }
        xmlhttp.send();
    }
    clearNum();
}
function clearNum()
{
    document.getElementById("numId").value = "";
    document.getElementById("numname").value = "";
    document.getElementById("numcAns").value = "";
    document.getElementById("numrange1").value = "";
    document.getElementById("numrange2").value = "";
    document.getElementById("numhint1").value = "";
    document.getElementById("numhint2").value = "";
    document.getElementById("numhint3").value = "";
}

function addNumElement(obj)
{
    var n =document.createTextNode(obj.question);                
    var li = createLi(obj.id);
    li.appendChild(n);
    
    var button = createEditButton("#numModal", obj.id);
    var xbutton = createDelButton(obj.id);

    var element;
    if(obj.section == "pre")
        element = document.getElementById("assessUL");
    else if(obj.section == "lab")
        element = document.getElementById("labUL");
    element.appendChild(xbutton);
    element.appendChild(li);
    element.appendChild(button);
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    console.log(allQuestions);
}

function shortCreate()
{
    if(document.getElementById("shortId").value == "")
    {
        var qname = document.getElementById("shortName").value;
        var id = Date.now();
        var question = {modId:currentMod, id:id, section:currentSection, type:"short", question:qname};

        allQuestions.push(question);
        addShortElement(question);
        
         console.log('Question : '+JSON.stringify(question));
        
        var url = "addquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
    }
    else
    {
        var id = document.getElementById("shortId").value;
        var question = getObj(id, allQuestions);
        
        question.question = document.getElementById("shortName").value;  
        
  /*      document.getElementById(id).innerHTML = "";
        document.getElementById(id).appendChild(document.createTextNode(question.question));*/
        
         var url = "editquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
                if(xmlhttp.responseText === 'true') {
                	console.log('Question Edited');
                	document.getElementById(id).innerHTML = "";
                	document.getElementById(id).appendChild(document.createTextNode(question.question));
                } else {
                	console.log('Question Not Edited');
                }
            }
        }
        xmlhttp.send();
    }
    clearShort();
}

function clearShort()
{
    document.getElementById("shortId").value = "";
    document.getElementById("shortName").value = "";
}
function addShortElement(obj)
{
    var n =document.createTextNode(obj.question);                
    var li = createLi(obj.id);
    li.appendChild(n);

    var button = createEditButton("#postLabModal", obj.id);
    var xbutton = createDelButton(obj.id);

    var element;
    if(obj.section == "pre")
        element = document.getElementById("assessUL");
    else if(obj.section == "lab")
        element = document.getElementById("labUL");
    else
        element = document.getElementById("postUL");
    element.appendChild(xbutton);
    element.appendChild(li);
    element.appendChild(button);
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    console.log(allQuestions);
}

function dataCreate()
{
    if(document.getElementById("dataId").value == "")
    {
        var qname = document.getElementById("dataName").value;
        var id = Date.now();
        var columnNames, i;
        var labels = document.getElementsByClassName("columnName")
        for(i = 0; i < labels.length; i++)
        {
            if(i == 0)
                columnNames = labels[i].value + ",";
            else if(i == labels.length - 1)
                columnNames = columnNames + labels[i].value;
            else
                columnNames = columnNames + labels[i].value + ",";
        }
        var question = {modId:currentMod, id:id, section:currentSection, type:"data", question:qname, units:document.getElementById("units").value, rows:document.getElementById("datarows").value, columns:document.getElementById("datacolumns").value, columnNames:columnNames};

        allQuestions.push(question);
        addDataElement(question);
        
        console.log('Question : '+JSON.stringify(question));
        
        var url = "addquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
            }
        }
        xmlhttp.send();
    }
    else
    {
        var id = document.getElementById("dataId").value;
        var question = getObj(id, allQuestions);
        
        var columnNames, i;
        var labels = document.getElementsByClassName("columnName")
        for(i = 0; i < labels.length; i++)
        {
            if(i == 0)
                columnNames = labels[i].value + ",";
            else if(i == labels.length - 1)
                columnNames = columnNames + labels[i].value;
            else
                columnNames = columnNames + labels[i].value + ",";
        }
        
        question.question = document.getElementById("dataName").value;
        question.units = document.getElementById("units").value;
        question.rows = document.getElementById("datarows").value;
        question.columns = document.getElementById("datacolumns").value;
        question.columnNames = columnNames;
        
         console.log('Question : '+JSON.stringify(question));
        
        var url = "editquestion.json?question="+JSON.stringify(question);
        xmlhttp.open('POST',url);
        xmlhttp.onreadystatechange = function() {
            if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                console.log(xmlhttp.responseText);
                if(xmlhttp.responseText === 'true') {
                	console.log('Question Edited');
                	document.getElementById(id).innerHTML = "";
                	document.getElementById(id).appendChild(document.createTextNode(question.question));
                } else {
                	console.log('Question Not Edited');
                }
            }
        }
        xmlhttp.send();
        
        /*document.getElementById(id).innerHTML = "";
        document.getElementById(id).appendChild(document.createTextNode(question.question));*/
    }
    clearData();
}

function clearData()
{
    document.getElementById("dataId").value = "";
    document.getElementById("dataName").value = "";
    document.getElementById("units").value = "";
    document.getElementById("datarows").value = "";
    document.getElementById("datacolumns").value = "";
    document.getElementById("dataEntryLabels").innerHTML = "";
}

function addDataElement(obj)
{
    var n =document.createTextNode(obj.question);                
    var li = createLi(obj.id);
    li.appendChild(n);

    var button = createEditButton("#dataEntryModal", obj.id);
    var xbutton = createDelButton(obj.id);

    var element = document.getElementById("labUL");

    element.appendChild(xbutton);
    element.appendChild(li);
    element.appendChild(button);
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    element.appendChild(document.createElement("BR"));
    console.log(allQuestions);
}

function addColumns()
{
    var col = document.getElementById("datacolumns").value;
    var colUL = document.getElementById("dataEntryLabels");
    colUL.innerHTML = "";
            	
    for(var i = 0; i < col; i++)
    {
        var input = document.createElement("INPUT");
        input.setAttribute("type", "text");
        input.setAttribute("class", "columnName");
        var br = document.createElement("BR");
        var br2 = document.createElement("BR");
        colUL.appendChild(input);
        colUL.appendChild(br);
        colUL.appendChild(br2);
    }     	
}

function editButtonPressed(id)
{
    var mod = getObj(id, modArray);
    if(mod != "")
    {
        document.getElementById("moduleId").value = id;
        document.getElementById("name").value = mod.question;
    }
    else
    {
        var question = getObj(id, allQuestions);
        if(question.type == 'mc')
        {
            document.getElementById("mcId").value = id;
            document.getElementById("mcname").value = question.question;
            document.getElementById("mcAns").value = question.cAns;
            document.getElementById("mciAns1").value = question.i1;
            document.getElementById("mciAns2").value = question.i2;
            document.getElementById("mciAns3").value = question.i3;
            document.getElementById("mchint1").value = question.h1;
            document.getElementById("mchint2").value = question.h2;
            document.getElementById("mchint3").value = question.h3;
        }
        else if(question.type == 'num')
        {
            document.getElementById("numId").value = id;
            document.getElementById("numname").value = question.question;
            document.getElementById("numcAns").value = question.cAns;
            document.getElementById("numrange1").value = question.r1;
            document.getElementById("numrange2").value = question.r2;
            document.getElementById("numhint1").value = question.h1;
            document.getElementById("numhint2").value = question.h2;
            document.getElementById("numhint3").value = question.h3;
        }
        else if(question.type == 'short')
        {
            document.getElementById("shortId").value = id;
            document.getElementById("shortName").value = question.question;
        }
        else
        {
            document.getElementById("dataId").value = id;
            document.getElementById("dataName").value = question.question;
            document.getElementById("units").value = question.units;
            document.getElementById("datarows").value = question.rows;
            document.getElementById("datacolumns").value = question.columns;
            
            var i;
            var arr = question.columnNames.split(",");
            var colUL = document.getElementById("dataEntryLabels");
            for(i = 0; i < arr.length; i++)
            {
                var input = document.createElement("INPUT");
                input.setAttribute("type", "text");
                input.setAttribute("class", "columnName");
                input.value = arr[i];
                var br = document.createElement("BR");
                var br2 = document.createElement("BR");
                colUL.appendChild(input);
                colUL.appendChild(br);
                colUL.appendChild(br2);
            }
        }
    }
}

function createEditButton(modaleName, id)
{
    var button = document.createElement("BUTTON");
    button.setAttribute("type", "button");
    button.setAttribute("class", "btn btn-default");
    button.setAttribute("data-toggle", "modal");
    button.setAttribute("data-target", modaleName);
    button.setAttribute("onclick", "editButtonPressed(" + id +")");
    button.setAttribute("style", "display:inline; float:right");
    var edit = document.createTextNode("Edit");
    button.appendChild(edit);
    return button;
}
function createLi(id)
{
    var li = document.createElement("LI");
    li.setAttribute("style", "display:inline; float:left");
    li.setAttribute("class", "list-group-item");
    li.setAttribute("id", id);
    return li;
}

function createDelButton(id)
{ 
    var button = document.createElement("BUTTON");
    button.setAttribute("type","button");
    button.setAttribute("class","close");
    button.setAttribute("style", "display:inline; float:left; padding: 0px 10px");
    button.setAttribute("onclick", "deleteButtonPressed(" + id +")");
    button.innerHTML = "&times;";
    return button;
}

function deleteButtonPressed(id)
{
    console.log("DELEt FNUCTON");
    console.log(id);
    console.log(filesArray);
    ///////////DELETE MODULE AND ASSOCIATED QUESTIONS////
    var mod = getObj(id, modArray);
    if(mod != "")
    {
        var i;
        var len = modArray.length;
        for(i = 0; i < len; i++)
        {
            if(modArray[i].id == id)
            {
                
                console.log('Delete Module'); 
                var url = "deletemodule.json?module="+JSON.stringify(mod);
                //var url = "deletemodule.json?id="+id;
                console.log('URL : '+url);
                
                xmlhttp.open('POST',url);
                xmlhttp.onreadystatechange = function() {
                if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                    console.log(xmlhttp.responseText);
                    modArray.splice(i, 1);
                    len--;
                    i--;
                    }
                }
                xmlhttp.send();
                
            }
        }
        len = allQuestions.length;
        for(i = 0; i < len; i++)
        {
            if(allQuestions[i].modId == id)
            {
                allQuestions.splice(i, 1);
                len--;
                i--;
            }
        }
        //////if the deleted mod was the current mod
        if(id == currentMod)
        {
            currentMod = 0;
            var names = document.getElementsByClassName("currentMod");
            var buttons = document.getElementsByClassName("qButton");
            for( i = 0; i < names.length; i++)
            {
                names[i].innerHTML = "No Module Selected";
                buttons[i].setAttribute("style", "display:none");
            } 
        }
    }
    ////////delete questions///////
    else
    {
        console.log("ELSE");
        var question = getObj(id, allQuestions);
        var i;
        var len = allQuestions.length;
        for(i = 0; i < len; i++)
        {
            if(allQuestions[i].id == id)
            {
                console.log('Delete Module');                
                var url = "deletequestion.json?question="+JSON.stringify(question);
                //var url = "deletequestion.json?id="+id;
                console.log('URL : '+url);
                
                xmlhttp.open('POST',url);
                xmlhttp.onreadystatechange = function() {
                if((xmlhttp.readyState === 4 || xmlhttp.readyState === 1) && (xmlhttp.status === 200 || xmlhttp.status === 0)) {
                    console.log(xmlhttp.responseText);
                    allQuestions.splice(i, 1);
                    len--;
                    i--;
                    }
                }
                xmlhttp.send();
                
            }
        }
    }
    console.log("END");
    console.log(allQuestions);
    ///////remove deleted item from view///////these should be in a div in the future
    var element = document.getElementById(id);
    element.parentNode.removeChild(element.previousSibling);
    element.parentNode.removeChild(element.nextSibling);
    element.parentNode.removeChild(element.nextSibling);
    element.parentNode.removeChild(element.nextSibling);
    element.parentNode.removeChild(element.nextSibling);
    element.parentNode.removeChild(element);
}


function AssignmentModules(assignmentName, modules, assignmentquestions) {
    this.assignmentName = assignmentName;
    this.modules = modules;
    this.assignmentquestions = assignmentquestions;
}

function AssignmentExperiment(assignmentName) {
    this.assignmentName = assignmentName;
}

function saveButton()
{
        var i;
    var a = document.getElementById("aName").value;
//    var d = document.getElementById("date").value;
//    var assignment = JSON.stringify({assignment: a});
    var mods = JSON.stringify(modArray);
    var quest = JSON.stringify(allQuestions);
    
    var assignMod = new AssignmentModules(a, modArray, allQuestions);
    var experiment = new AssignmentExperiment(a);
    
    console.log('Assign Mod : ');
    console.log(JSON.stringify(experiment));
    
    console.log('Questtions all : ');
    console.log(quest);
    
   // var url = "createassignment.json?assignment="+JSON.stringify(assignMod);
    var url = "createexperiment.json?experiment="+JSON.stringify(experiment);
    
    console.log('JSON');
    console.log(JSON.stringify(a));
    xmlhttp.open("POST",url , true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    console.log(xmlhttp);
    xmlhttp.onreadystatechange = function() {
    	console.log("URL : ");
        if((xmlhttp.status === 200 || xmlhttp.status === 0) && (xmlhttp.readyState === 4 || xmlhttp.readyState === 1)) {
            console.log(xmlhttp.responseText);
            
       /*     var url2 = "createquestions.json?questions="+quest;
            xmlhttp.open("POST",url , true);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            console.log(xmlhttp);
            xmlhttp.onreadystatechange = function() {
            	console.log("URL : "+url);
                if((xmlhttp.status === 200 || xmlhttp.status === 0) && (xmlhttp.readyState === 4 || xmlhttp.readyState === 1)) {
                	console.log("Questions Received");
                }
            }*/
        }
    }
    xmlhttp.send();
    

}    