<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head> 
	<body>
		<h2>${message}</h2>
		    <button id="btn-click">Click</button>

		<script type="text/javascript">
		
		document.getElementById("btn-click").onclick = function () {
//		window.onload = function () {
		    
		    var request = new XMLHttpRequest();
		    
		    var url = "prelab.json";
		    request.open("GET", url);
		    
		    request.onreadystatechange = function () {
		        console.log(request);
		        if((request.readyState === 1 || request.readyState === 4) && (request.status === 200 || request.status === 0)) {
		            console.log('Prelab : '+request.responseText);
		        }
		    }
		    request.send();

		    var url = "lab.json";
		    request.open("GET", url);
		    
		    request.onreadystatechange = function () {
		        console.log(request);
		        if((request.readyState === 1 || request.readyState === 4) && (request.status === 200 || request.status === 0)) {
		            console.log('Lab : '+request.responseText);
		        }
		    }
		    request.send();

		    var url = "postlab.json";
		    request.open("GET", url);
		    
		    request.onreadystatechange = function () {
		        console.log(request);
		        if((request.readyState === 1 || request.readyState === 4) && (request.status === 200 || request.status === 0)) {
		            console.log('Postlab : '+request.responseText);
		        }
		    }
		    request.send();

		    
		}
		</script>
	</body>
</html>
