<html lang="en">
<head>
  <title>Create Assignment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
     <script src="<%=request.getContextPath()%>/resources/js/createassignment.js"></script>
</head>
    
<body>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="indexI.html">LABCreator</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="indexI.html">Home</a></li>
      <li><a href="#">Roster</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Logout</a></li> 
    </ul>
  </div>
</nav>
<br>
    <div class="form-group" style="padding:12px 12px">
        <label for="aName">Assignment Name:</label>
        <input type="text" class="form-control" id="aName">
    </div>
    
<br>
<div class="container">
  <ul class="nav nav-tabs nav-justified">
  	<li class="active"><a data-toggle="tab" href="#mod">Module</a></li>
    <li><a data-toggle="tab" href="#assess">Pre-Lab</a></li>
    <li><a data-toggle="tab" href="#lab">Lab</a></li>
    <li><a data-toggle="tab" href="#post">Post-Lab</a></li>
  </ul>

  <div class="tab-content">
      <br>
    <div id="mod" class="tab-pane fade in active">
        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#modModal">Create Module</button>
    <br><br>
      <div>
        <!--MODULO ELEMENTS ADDED HERE VIA SCRIPT-->
         <ul class="list-group" id="modUL">
          </ul>
          <br><br><br>
        </div>
    </div>

    <div id="assess" class="tab-pane fade">
        <h5 class="currentMod">No Module Selected</h5>
        <div class="qButton" style="display:none">        
       <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#mcModal" onclick="setSection('pre')">Multiple Choice</button>
        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#numModal" onclick="setSection('pre')">Numerical</button>	
        </div>
        <br>
        <div>
            <ul class="list-group" id="assessUL">
            </ul>
        </div>
    </div>
      
    <div id="lab" class="tab-pane fade">
        <h5 class="currentMod">No Module Selected</h5>
        <div class="qButton" style="display:none"> 
            <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#mcModal" onclick="setSection('lab')">Multiple Choice</button>
            <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#numModal" onclick="setSection('lab')">Numerical</button>
            <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#postLabModal" onclick="setSection('lab')">Short Answer</button>
            <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#dataEntryModal" onclick="setSection('lab')">Data Entry</button>
        </div>
        <br>
        <div>
            <ul class="list-group" id="labUL">
            </ul>
        </div>
    </div>
      
    <div id="post" class="tab-pane fade">
        <h5 class="currentMod">No Module Selected</h5>
        <div class="qButton" style="display:none"> 
       <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#postLabModal" onclick="setSection('post')">Short Answer</button>
    </div>
      <br>
      <div>
          <ul class="list-group" id="postUL">
          </ul>
      </div>
  </div>
 </div>
</div>

    <br>
<div style="padding:12px 12px">
    <button class="btn btn-default btn-lg" style="float:right" onclick="saveButton()">Save</button>
</div>
    
<!-- #########################################BREAK###################################################-->
    
    <!-- ModulocModal -->
<div id="modModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="clearMod()">&times;</button>
        <h4 class="modal-title">New Modulo</h4>
      </div>
      <div class="modal-body">
        <label>Name:</label>
        <input type="text" class="form-control" id="name" name="name">
        <br>
          <label>Please select a PDF file to include with the lab:</label>
        <input type="file" accept="application/pdf" id="file" name="file">
        <input type="hidden" id="moduleId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearMod()">Close</button>
          <button type="button" class="btn btn-primary" id="createMod" data-dismiss="modal" onclick="modCreate()">Save</button>
      </div>
    </div>  
  </div>
</div>
    
       <!-- MCModal -->
<div id="mcModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="clearMC()">&times;</button>
        <h4 class="modal-title">Multiple Choice Question</h4>
      </div>
      <div class="modal-body">
        <label>Question:</label>
        <input type="textarea" class="form-control" id="mcname">
        <br>
          <label>Correct Answer:</label>
        <input type="text" class="form-control" id="mcAns">
          <br>
          <label>Incorrect Answers:</label>
            <input type="text" class="form-control" id="mciAns1">
            <input type="text" class="form-control" id="mciAns2">
            <input type="text" class="form-control" id="mciAns3">
          <br>
          <label>Hints:</label>
<!--             <input type="text" class="form-control" id="mchint1">
            <input type="text" class="form-control" id="mchint2">
            <input type="text" class="form-control" id="mchint3"> -->
            <textarea rows="4" cols="50" class="form-control" id="mchint1"></textarea>
            <textarea rows="4" cols="50" class="form-control" id="mchint2"></textarea>
            <textarea rows="4" cols="50" class="form-control" id="mchint3"></textarea>
            <input type="hidden" id="mcId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearMC()">Close</button>
        <button type="button" class="btn btn-primary" id="createMC" data-dismiss="modal" onclick="mcqCreate()">Save</button>
      </div>
    </div>  
  </div>
</div>
    
          <!-- NUMModal -->
<div id="numModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="clearNum()">&times;</button>
        <h4 class="modal-title">Numerical Question</h4>
      </div>
      <div class="modal-body">
        <label>Question:</label>
        <input type="textarea" class="form-control" id="numname">
        <br>
          <label>Correct Answer:</label>
        <input type="number" class="form-control" id="numcAns">
          <br>
          <label>Range:</label>
            <input type="number" class="form-control" id="numrange1">
            <input type="number" class="form-control" id="numrange2">
          <br>
          <label>Hints:</label>
           <!--  <input type="text" class="form-control" id="numhint1">
            <input type="text" class="form-control" id="numhint2">
            <input type="text" class="form-control" id="numhint3"> -->
            <textarea rows="4" cols="50" class="form-control" id="numhint1"></textarea>
            <textarea rows="4" cols="50" class="form-control" id="numhint2"></textarea>
            <textarea rows="4" cols="50" class="form-control" id="numhint3"></textarea>
            <input type="hidden" id="numId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearNum()">Close</button>
        <button type="button" class="btn btn-primary" id="createNum" data-dismiss="modal" onclick="numCreate()">Save</button>
      </div>
    </div>  
  </div>
</div>

          <!-- PostLabModal -->
<div id="postLabModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="clearShort()">&times;</button>
        <h4 class="modal-title">Short Answer Question</h4>
      </div>
      <div class="modal-body">
        <label>Question:</label>
        <input type="textarea" class="form-control" id="shortName">
        <input type="hidden" id="shortId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearShort()">Close</button>
        <button type="button" class="btn btn-primary" id="createShort" data-dismiss="modal" onclick="shortCreate()">Save</button>
      </div>
    </div>  
  </div>
</div>

         <!-- DataEntryModal -->
<div id="dataEntryModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="clearData()">&times;</button>
        <h4 class="modal-title">Data Entry Question</h4>
      </div>
      <div class="modal-body">
        <label>Question/Instructions:</label>
        <input type="textarea" class="form-control" id="dataName">
         <label>Units:</label>
        <input type="text" class="form-control" id="units">
        <label>Number of Trials:</label>
        <input type="number" class="form-control" id="datarows">
        <label>Number of Data Entry Points(e.g. intial mass, final mass, ect.):</label>
        <input type="number" class="form-control" id="datacolumns">
        <input type="hidden" id="dataId">
        <button type="button" class="btn btn-default" onclick="addColumns()">Add</button>
        <br>
        <label>Data Entry Labels:</label>
        <ul id="dataEntryLabels"></ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearData()">Close</button>
<button type="button" class="btn btn-primary" id="createData" data-dismiss="modal" onclick="dataCreate()">Save</button>
      </div>
    </div>  
  </div>
</div>  
</body>
</html>