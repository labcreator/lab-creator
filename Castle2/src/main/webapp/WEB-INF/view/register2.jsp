<html lang="en">
<head>
  <title>Register</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--<link rel="stylesheet" href="reg.css">-->
    
    <style type="text/css">
        #netid, #firstname, #lastname
            {
                width: 30%;
                display: inline-block;
            }
        
        .container { margin-top: 5em;}
    </style>
</head>
<body>
     
<div class="container">
  <h2 style="text-align:center">Register</h2>
    <br><br>
<!--  <form role="form" id="form" method="post" action="register_post.html">-->
      <div style="text-align:center">
	${status}
          </div>
    <div class="form-group" style="text-align:center">
      <input type="text" class="form-control" id="netid" name="netid" placeholder="Net ID">
    </div>
      <div style="text-align:center">
      </div>
    <div class="form-group" style="text-align:center">
      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
    </div>
       <div style="text-align:center">
      </div>
    <div class="form-group" style="text-align:center">
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
    </div>
<!--     <div class="form-group" style="text-align:center">
      <select id="usertype" name="usertype">
      	<option value="student">Student</option>
    	<option value="instructor">Instructor</option>
      </select>
    </div>-->

      <div style="text-align:center">
           <button type="submit" class="btn btn-default btn-lg" id="submit">Register</button>
      </div>
<!--  </form>-->
    <div style="text-align:center">
        <a href="login.html">Already Have an Account? Login Here!</a>
    </div>
</div>

</body>    
</html>

 <script type="text/javascript">
        var xmlrequest = new XMLHttpRequest();

        window.onload = function() {
            
            var netid, fname, lname, usertype;
          /*  
            function registerDetails(netid, fname, lname, usertype) {
                this.netid = netid;
                this.fname = fname;
                this.lname = lname;
                this.usertype = usertype;
            }
            */
             function registerDetails(netid, fname, lname) {
                this.netid = netid;
                this.fname = fname;
                this.lname = lname;
            }
            
            document.getElementById("submit").onclick = function() {
            
                var netid = document.getElementById("netid"),
                    fname = document.getElementById("firstname"),
                    lname = document.getElementById("lastname");
                
                console.log("NETID : "+netid.value);
                console.log("F NAME : "+fname.value);
                console.log("L NAME : "+lname.value);
                
          /*      var user = document.getElementById("usertype");
                console.log("User type : "+user.value);*/
                                
                var register = new registerDetails(netid.value, fname.value, lname.value);
                console.log("register : "+JSON.stringify(register));


                var url = "register.json?register="+JSON.stringify(register);
                //xmlrequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlrequest.open("POST", url);
     
                console.log(xmlrequest);
               /*  xmlrequest.onreadystatechange = function() {
                    
                    console.log("Inside onreadystate change");
                    if((xmlrequest.readyState === 4 || xmlrequest.readyState === 1) && (xmlrequest.status === 200 || xmlrequest.status === 0)) {
                        console.log("Response successful");
                        console.log(xmlrequest.responseText);
                        console.log(JSON.parse(xmlrequest.responseText));
                        window.location.href=JSON.parse(xmlrequest.responseText);
                    }
                
                } */
                xmlrequest.send();
            }
        }
</script>