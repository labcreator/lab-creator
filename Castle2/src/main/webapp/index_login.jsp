<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 <!--   <link rel="stylesheet" href="login.css">-->
    
    <style type="text/css">
        #netid, #firstname
        {
            width: 30%;
            display: inline-block;
        }

        .container { margin-top: 10em;}
        </style>

</head>
<body>
     
<div class="container">
  <h2 style="text-align:center">Login</h2>
    <br><br>
<!--  <form role="form" id="form" method="POST" action="authenticate.html">-->
      <div style="text-align:center">
<!--		${status}-->
          </div>
    <div class="form-group" style="text-align:center">
      <input type="text" class="form-control" id="netid" name="netid" placeholder="Net ID">
    </div>
      <div style="text-align:center">
      </div>
    <div class="form-group" style="text-align:center">
      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
    </div>
      <div class="form-group" style="text-align:center">
      <select id="usertype" name="usertype">
      	<option value="student">Student</option>
    	<option value="instructor">Instructor</option>
      </select>
    </div>
      <div style="text-align:center">
           <button type="submit" class="btn btn-default btn-lg" id="submit">Login</button>
      </div>
<!--  </form>-->
    <div style="text-align:center">
        <a href="register.html">First Time User? Register Here!</a>
    </div>
</div>
    <script type="text/javascript">
        var xmlrequest = new XMLHttpRequest();

        window.onload = function() {
            
            var netid, name, usertype;
            
            function loginDetails(netid, name, usertype) {
                this.netid = netid;
                this.name = name;
                this.usertype = usertype;
            }
            
            document.getElementById("submit").onclick = function() {
            
                var netid = document.getElementById("netid"),
                    fname = document.getElementById("firstname");
                
                console.log("NETID : "+netid.value);
                console.log("F NAME : "+fname.value);
                
                var user = document.getElementById("usertype");
                console.log("User type : "+user.value);
                                
                var login = new loginDetails(netid.value, fname.value, user.value);
                console.log("LOgin : "+JSON.stringify(login));


                var url = "login.json?login="+JSON.stringify(login);
            //    xmlrequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlrequest.open("POST", url);
     
                console.log(xmlrequest);
                xmlrequest.onreadystatechange = function() {

                    if((xmlrequest.readyState === 4 || xmlrequest.readyState === 1) && (xmlrequest.status === 200 || xmlrequest.status === 0)) {
                        console.log("Response successful");
                    }
                
                }
                xmlrequest.send();
            }
        }
    </script>
</body>
</html>

