var createdAssign ={};

window.onload = function(){
    if(sessionStorage.User == null || sessionStorage.User == 'null')
    {
        firebase.auth().signOut();
        window.location = "/login.html";
    }
}

function newAssign()
{
    var id = Date.now();
    window.location = "/createAssignment.html?id=" + id;
}