function logout()
 {
     firebase.auth().signOut();
     sessionStorage.User = null;
     window.location = "/homepage.html";
 }