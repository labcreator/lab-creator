window.onload = function(){
    console.log("WINDOW LOASD");
    if(sessionStorage.User == null || sessionStorage.User == 'null')
        firebase.auth().signOut();
    else 
        window.location = "/homepage.html";  
}

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
      // User is signed in.
      console.log("SIGNED IN");
      if(JSON.parse(sessionStorage.Active) == null)
          firebase.auth().signOut();
      else
      {
        sessionStorage.User = firebase.auth().currentUser.email;
        window.location = "/homepage.html";
      }
  } else {
      console.log("NOT LOGGED IN");
    // No user is signed in.
     // sessionStorage.User = null;
  }
});

function submitFunction()
{
    sessionStorage.Active = 1;
    document.getElementById("errorMessage").innerHTML = "";
    var email = document.getElementById("email").value; 
    var password = document.getElementById("pw").value;
 
    if(email == "" || email == null)
      window.alert("Error: Please enter an email");
    else if(password == "" || password == null)
      window.alert("Error: Please enter a password");
    else 
    {
        console.log("else");
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here.  
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);
            console.log(errorCode);
            document.getElementById("errorMessage").appendChild(document.createTextNode("Error: Invalid Username or Password!"));
            // ...
        });
    }
}
