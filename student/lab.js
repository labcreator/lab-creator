var curPage; ////set in page load
var lastPage;
var questionArray;
var allSections = [];
var currentSection;
var sectionName;
var curQuestion; ////id of current question
var hints = [];
var attempts = 0;
var curHint;
///////datatable variables//////////
var varVal;
var varEq;
var varPos;
var varExtra;
var dataObj;
/////////prelab hypo questions/////
var hypoQuestions = [];
////////pdf name, lab name//////
var pdfName;
var labName;
/*var x = "Infinity";
var y = "Infinity";
console.log(math.isNumeric(x));
console.log(math.eval("std(12, 17, 20, 25, 27)", {'x':x, 'y':"12"}));
console.log(math.eval("(std(12, 17, 20, 25, 27)/mean(12, 17, 20, 25, 27))*1000"));  */          
var xmlhttp = new XMLHttpRequest();
var url = "acidlab.json";
xmlhttp.open('GET', url, true);
xmlhttp.send();
xmlhttp.onreadystatechange = function()
{
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if(loadNull(sessionStorage.getItem("refresh", "num")) != 1)
        {
            console.log("HHTP REQUEST");
            var json = JSON.parse(xmlhttp.responseText);
            console.log(json.questions);
            console.log(json.dataTable);
            pdfName = json.pdf;
            labName = json.name;
            setupQuestions(json.questions);
            sectionName = json.section;
            console.log(sectionName);
            if(sectionName == "lab")
            {
                console.log("TEST");
                setupDataTable(json.dataTable);
                dataObj = json.dataTable;
            }
            curPage = 1;
            lastPage = 1;
        }
    }
};

///////load and unload fucntions/////
window.onbeforeunload = function()
{
    console.log("UNLOAD TEST");
    sessionStorage.setItem("refresh", JSON.stringify(1));
    sessionStorage.setItem("curPage", JSON.stringify(curPage));
    sessionStorage.setItem("lastPage", JSON.stringify(lastPage));
    sessionStorage.setItem("questionArray", JSON.stringify(questionArray));
    sessionStorage.setItem("sectionName", JSON.stringify(sectionName));
    sessionStorage.setItem("curQuestion", JSON.stringify(curQuestion));
    sessionStorage.setItem("attempts", JSON.stringify(attempts));
    sessionStorage.setItem("objective", JSON.stringify(document.getElementById("objective").value));
    sessionStorage.setItem("hyp", JSON.stringify(document.getElementById("hyp").value));
    sessionStorage.setItem("var", JSON.stringify(document.getElementById("var").value));
    sessionStorage.setItem("exo", JSON.stringify(document.getElementById("exo").value));
    sessionStorage.setItem("chh", JSON.stringify(document.getElementById("chh").value));
    sessionStorage.setItem("pdf", JSON.stringify(pdfName));
    sessionStorage.setItem("name", JSON.stringify(labName));
    ////datatable storage////
    dataObj.varVal = varVal;
    sessionStorage.setItem("dataTable", JSON.stringify(dataObj));
}

window.onload = function()
{
    console.log("LOAD TEST");
    if(loadNull(sessionStorage.getItem("refresh", "num")) == 1)
    {
        var num;
        curPage = loadNull(sessionStorage.getItem("curPage"), "num");
        lastPage = loadNull(sessionStorage.getItem("lastPage"), "num");
        pdfName = loadNull(sessionStorage.getItem("pdf"), "str");
        labName = loadNull(sessionStorage.getItem("name"), "str");
        questionArray = loadNull(sessionStorage.getItem("questionArray"), "arr");
        setupQuestions(questionArray);
        sectionName = loadNull(sessionStorage.getItem("sectionName"), "str");
        //currentSection = getSection(sectionName);
        curQuestion = loadNull(sessionStorage.getItem("curQuestion"), "num");
        attempts = loadNull(sessionStorage.getItem("attempts"), "num");
        curHint = getObj(curQuestion, hints);
        if(sectionName == "hypo")
        {
            document.getElementById("questionView").setAttribute("style", "display:none");
            document.getElementById("hypoSection").setAttribute("style", "display:block");
           document.getElementById("question").removeChild(document.getElementById(currentSection[0].id));
            
            document.getElementById("objective").value = loadNull(sessionStorage.getItem("objective"), "str");
            document.getElementById("hyp").value = loadNull(sessionStorage.getItem("hyp"), "str");
            document.getElementById("var").value = loadNull(sessionStorage.getItem("var"), "str");
            document.getElementById("exo").value = loadNull(sessionStorage.getItem("exo"), "str");
            document.getElementById("chh").value = loadNull(sessionStorage.getItem("chh"), "str");
        }
        else
        {
            for(num = 1; num < curPage; num++)
            {
                var li = document.createElement("LI");
                li.setAttribute("class", "");
                li.setAttribute("id", num+1);
                li.setAttribute("onclick", "pageClick("+(num+1)+")");
                var a = document.createElement("A");
                a.setAttribute("href", "#");
                a.setAttribute("data-toggle", "tab");
                var text = document.createTextNode(num+1);

                a.appendChild(text);
                li.appendChild(a);
                document.getElementById("pageUL").appendChild(li);
            }
            
            document.getElementById("question").removeChild(document.getElementById(currentSection[0].id));
            document.getElementById("question").appendChild(currentSection[curPage-1].div);
            document.getElementById("1").setAttribute("class", "");
            document.getElementById(curPage).setAttribute("class", "active");
            
            if(sectionName == "lab")
            {
                dataObj = JSON.parse(sessionStorage.getItem("dataTable"));
                varVal = dataObj.varVal;
                varEq = dataObj.varEq;
                varPos = dataObj.varPos;
                varExtra = dataObj.varExtra;
                setupDataTable(dataObj);
                document.getElementById("dataTable").setAttribute("style", "display:block");
                fillData();
            }
            ////add values to table////
        }
    }
}

function getSection(name)
{
    if(name == "pre")
        return allSections[0];
    else if(name == "lab")
        return allSections[1];
    else if(name == "post")
        return allSections[2];
    else 
        return allSections[0];
}

function setupQuestions(quest)
{
    questionArray = quest;
    console.log(quest);
    var i;
    var arr = [];
    for(i = 0; i < quest.length; i++)
    {
        arr.push(createQuestion(quest[i]));
    }
    /*allSections.push(pre);
    allSections.push(lab);
    allSections.push(post);
    console.log(allSections);*/
    currentSection = arr;
    document.getElementById("question").appendChild(currentSection[0].div);
    curQuestion = currentSection[0].id;  
    curHint = getObj(curQuestion, hints);
    document.getElementById("next").setAttribute("disabled", "disabled");
    //sectionName = "pre";
    ///////setup pdf file//////////
    console.log(pdfName);
    var embed = document.createElement("EMBED");
    embed.setAttribute("src", pdfName);
    document.getElementById("pdf").appendChild(embed);
    
    document.getElementById("modName").innerHTML = labName;
}

function createQuestion(qObj)
{
    var i;
    var div = document.createElement("div");
    div.setAttribute("id", qObj.id);
    var p = document.createElement("h5");
    p.appendChild(document.createTextNode(qObj.question));
    div.appendChild(p);
    console.log("createQ");
    if(qObj.type == "mc") ///////add a check for less then 4 mutliple choice answers
    {
        var form = document.createElement("FORM");
        form.setAttribute("style", "text-align:left");
        var c = createMCElement(qObj.cAns);
        var i1 = createMCElement(qObj.i1);
        var arr = [c, i1];
        if(qObj.i2 != "")
            arr.push(createMCElement(qObj.i2));
        if(qObj.i3 != "")
        arr.push(createMCElement(qObj.i3));
        arr = shuffleArray(arr);
        for(i = 0; i < arr.length; i++)
            form.appendChild(arr[i]);
        div.appendChild(form);
    }
    else if(qObj.type == "num")
    {
        var numDiv = document.createElement("DIV");
        numDiv.setAttribute("style", "text-align:left");
        var text = document.createElement("LABEL");
        text.setAttribute("for", "ans");
        text.appendChild(document.createTextNode("Answer: "));
        var input = document.createElement("INPUT");
        input.setAttribute("id", "ans");
        input.setAttribute("type", "number");
        input.setAttribute("class", "form-control");
        input.setAttribute("style", "width:30%");
        numDiv.appendChild(text);
        numDiv.appendChild(input);
        numDiv.appendChild(document.createElement("BR"));
        div.appendChild(numDiv);
    }
    else if(qObj.type == "short")
    {
        var shortDiv = document.createElement("DIV");
        shortDiv.setAttribute("style", "text-align:left");
        shortDiv.setAttribute("class", "form-group");
        var text = document.createElement("LABEL");
        text.setAttribute("for", "ans");
        text.appendChild(document.createTextNode("Answer: "));
        var input = document.createElement("TEXTAREA");
        input.setAttribute("id", "ans");
        input.setAttribute("rows", "5");
        input.setAttribute("class", "form-control");
        shortDiv.appendChild(text);
        shortDiv.appendChild(input);
        shortDiv.appendChild(document.createElement("BR"));
        div.appendChild(shortDiv);
    }
    else if(qObj.type == "data")
    {
       // console.log("data");
        var i, j;
        var dataDiv = document.createElement("DIV");
        dataDiv.setAttribute("class", "table-responsive");
        //dataDiv.setAttribute("style", "text-align:left");
        var c = qObj.columns;
        var r = qObj.rows;
        var arr = qObj.columnNames.split(",");
        var table= document.createElement("TABLE");
        table.setAttribute("class", "table table-striped table-condensed");
        var header = document.createElement("THEAD");
        var row = document.createElement("TR");
        var trial = document.createElement("TH");
        trial.innerHTML = "Trial";
        row.appendChild(trial);
        for(i = 0; i < arr.length; i++)
        {
            var th = document.createElement("TH");
            th.appendChild(document.createTextNode(arr[i]));
            row.appendChild(th);
        }
        header.appendChild(row);
        table.appendChild(header);
        var body = document.createElement("TBODY");
        for(i = 0; i < r; i++)
        {
            var name = "de" + i;
            row = document.createElement("TR");
            var td = document.createElement("TD");
            td.appendChild(document.createTextNode((i+1)+": "));
            row.appendChild(td);
            for(j = 0; j < c; j++)
            {
               // var id = qObj.dVars[i][j];
                var t = document.createElement("TD");
                var input = document.createElement("INPUT");
                input.setAttribute("style", "width: 60%");
                input.setAttribute("type", "text");
                input.setAttribute("name", name);
                //input.setAttribute("id", id);
                var units = document.createTextNode(qObj.units);
                t.appendChild(input);
                t.appendChild(units);
                row.appendChild(t);
              //  console.log(id);
            }
            var tdb = document.createElement("TD");
            var button = document.createElement("BUTTON");
            button.setAttribute("class", "btn btn-default");
            button.setAttribute("onclick", "dataVal('"+name+"',"+i+")");
            button.innerHTML = "Check";
            tdb.appendChild(button);
            row.appendChild(tdb);
            body.appendChild(row);
            console.log(row);
        }
        table.appendChild(body);
        dataDiv.appendChild(table);
        dataDiv.appendChild(document.createElement("BR"));
        //div.appendChild(table);
        div.appendChild(dataDiv);
    }
            
    //////////setup hints/////////////
    console.log(qObj.h1);
    console.log(qObj.h2);
    console.log(qObj.h3);
    var h1 = document.createElement("P");
    var h2 = document.createElement("P");
    var h3 = document.createElement("P");
    h1.appendChild(document.createTextNode(qObj.h1));
    if(qObj.h1Im != null)
    {
        h1.appendChild(document.createElement("BR"));
        var img1 = document.createElement("IMG");
        img1.setAttribute("src", qObj.h1Im);
        img1.setAttribute("style", "width: 100%; height: auto;");
        h1.appendChild(img1);
    }
    h1.appendChild(document.createElement("BR"));
    h2.appendChild(document.createTextNode(qObj.h2));
    if(qObj.h2Im != null)
    {
        h2.appendChild(document.createElement("BR"));
        var img2 = document.createElement("IMG");
        img2.setAttribute("src", qObj.h2Im);
        img2.setAttribute("style", "width: 100%; height: auto;");
        h2.appendChild(img2);
    }
    h2.appendChild(document.createElement("BR"));
    h3.appendChild(document.createTextNode(qObj.h3));
    if(qObj.h3Im != null)
    {
        h3.appendChild(document.createElement("BR"));
        var img3 = document.createElement("IMG");
        img3.setAttribute("src", qObj.h3Im);
        img3.setAttribute("style", "width: 100%; height: auto;");
        h3.appendChild(img3);
    }
    h3.appendChild(document.createElement("BR"));
    hints.push({id:qObj.id, h1:h1, h2:h2, h3:h3}); 
    return {id:qObj.id, div:div};
}

function createMCElement(ans)
{
    var radioDiv = document.createElement("DIV");
    radioDiv.setAttribute("class", "radio");
    var radio = document.createElement("INPUT");
    radio.setAttribute("type", "radio");
    radio.setAttribute("name", "ans");
    radio.setAttribute("value", ans);
    var l1 = document.createElement("LABEL");
    l1.appendChild(radio);
    l1.appendChild(document.createTextNode(ans));
    l1.appendChild(document.createElement("BR"));   
    radioDiv.appendChild(l1);
    return radioDiv;
}
//////////////////////////////////////////
function nextQuestion()
{///////check for the end of the section///////
    if(curPage == currentSection.length)
    {
        console.log("END OF THE SECTION");
        setNextSection();
    }
    else
    {
        createPage(curPage+1);

        document.getElementById("next").setAttribute("disabled", "disabled");
        document.getElementById("submit").removeAttribute("disabled");
        document.getElementById("hints").innerHTML = "";
        document.getElementById("question").removeChild(document.getElementById(curQuestion));
        document.getElementById("question").appendChild(currentSection[curPage].div);
        document.getElementById("error").innerHTML = "";
        document.getElementById("valid").innerHTML = "";
        
        curQuestion = currentSection[curPage].id;
        attempts = 0;
        curPage++;
        lastPage = curPage;
        curHint = getObj(curQuestion, hints);
    }
}

function setNextSection()
{
    document.getElementById("error").innerHTML = "";
    document.getElementById("valid").innerHTML = "";
    document.getElementById("hints").innerHTML = "";
    if(sectionName == "pre")
    {
        document.getElementById("questionView").setAttribute("style", "display:none");
        document.getElementById("hypoSection").setAttribute("style", "display:block");
        sectionName = "hypo";
        
        //////clear question section////
        document.getElementById("question").removeChild(document.getElementById(curQuestion));
        document.getElementById("pageUL").innerHTML = "";
        curPage = 1;
        lastPage = curPage;
        createPage(curPage);
    }
    else if(sectionName == "hypo")
    {
        hypoQuestions.push(document.getElementById("objective").value);
        hypoQuestions.push(document.getElementById("hyp").value);
        hypoQuestions.push(document.getElementById("var").value);
        hypoQuestions.push(document.getElementById("exo").value);
        hypoQuestions.push(document.getElementById("chh").value);
        
        
        /*document.getElementById("questionView").setAttribute("style", "display:block");
        document.getElementById("hypoSection").setAttribute("style", "display:none");   
        sectionName = "lab";
                
        currentSection = allSections[1];
        document.getElementById("question").appendChild(currentSection[0].div);
        curQuestion = currentSection[0].id;  
        curHint = getObj(curQuestion, hints);
        document.getElementById("next").setAttribute("disabled", "disabled");
        document.getElementById("submit").removeAttribute("disabled");
        //////////show data table////////////
        document.getElementById("dataTable").setAttribute("style", "display:block");*/
        //////splash page/////////////
        savePDF("hypo");
    }
    else 
        savePDF("");
    ////////////page should redirect to finished.html from server////////////
}

function validateHypo()
{
    var err = document.getElementById("hypoError");
    err.innerHTML = "";
    if(document.getElementById("objective").value == "")
        err.appendChild(document.createTextNode("Please Enter an Objective!"));
    else if(document.getElementById("hyp").value == "")
        err.appendChild(document.createTextNode("Please Enter a Hypothesis!"));
    else if(document.getElementById("var").value == "")
        err.appendChild(document.createTextNode("Please Enter Experiment Variables!"));
    else if(document.getElementById("exo").value == "")
        err.appendChild(document.createTextNode("Please Enter an Experiment Outline!"));
    else if(document.getElementById("chh").value == "")
        err.appendChild(document.createTextNode("Please Enter any Chemical Hazards!"));
    else 
    {
        setNextSection();
    }//////move on to the next section/////
    window.scrollTo(0,0);
}

function createPage(num)
{
    var li = document.createElement("LI");
    li.setAttribute("class", "active");
    li.setAttribute("id", num);
    li.setAttribute("onclick", "pageClick("+num+")");
    var a = document.createElement("A");
    a.setAttribute("href", "#");
    a.setAttribute("data-toggle", "tab");
    var text = document.createTextNode(num);
    
    a.appendChild(text);
    li.appendChild(a);
    document.getElementById("pageUL").appendChild(li);
    document.getElementById(curPage).setAttribute("class", "");
    console.log(document.getElementById(curPage));
}

function pageClick(id)
{
    console.log(curPage);
    console.log(id);
    console.log(lastPage);
    document.getElementById("question").removeChild(document.getElementById(curQuestion));
    curPage = id;
    curQuestion = currentSection[curPage-1].id;
    if(id != lastPage)
    {
        document.getElementById("next").setAttribute("disabled", "disabled");
        document.getElementById("submit").setAttribute("disabled", "disabled");
    }
    else
        document.getElementById("submit").removeAttribute("disabled");
    
    document.getElementById("question").appendChild(currentSection[curPage-1].div);
}

function validate()
{
    var obj = getObj(curQuestion, questionArray);
    var bool;
    
    if(obj.type == "mc")
    {
        var i;
        var ans = document.getElementsByName("ans");
        for(i = 0; i < ans.length; i++)
        {
            if(ans[i].checked)
            {
                if(ans[i].value == obj.cAns)
                    bool = true;
                else
                    bool = false;
            }
        }
    }
    else if(obj.type == "num")
    {
        var ans = parseFloat(document.getElementById("ans").value, 10);
        var r1 = parseFloat(obj.r1, 10);
        var r2 = parseFloat(obj.r2, 10);
        if(r1 <= ans && ans <= r2)
            bool = true;
        else if(ans < r1 || ans > r2)
            bool = false; 
    }
    else if(obj.type == "short")
    {
        if(document.getElementById("ans").value == "")
        {
            displayMes(null);
            return;
        }
        else
        {
            displayMes(true);
            bool = true;
        }
    }
    else if(obj.type == "data")
    {
        calcExtras();
        bool = true;
    }
    displayMes(bool);
    
    if(bool == true)
    {
        document.getElementById("next").removeAttribute("disabled");
        document.getElementById("submit").setAttribute("disabled", "disabled");
        var q = getObj(curQuestion, questionArray);
        if(obj.type != "short")
            q.attempts = attempts;
        else
            q.attempts = document.getElementById("ans").value;
        //////////////save short answer values///////////
    }
    else if(bool == false)
    {/////make hints the active tab////
        document.getElementById("hintsTab").setAttribute("class", "active");
        document.getElementById("pdfTab").setAttribute("class", "");
        document.getElementById("pdf").setAttribute("class", "tab-pane fade");
        document.getElementById("hintsBlock").setAttribute("class", "tab-pane fade in active");
        var div = document.getElementById("hints");
        var h = document.createElement("H4");
        console.log(attempts);
        console.log(curHint);
        console.log(curHint.h1);
        if(attempts == 0)
        {
            h.appendChild(document.createTextNode("Hint 1:"))
            div.appendChild(h);
            div.appendChild(curHint.h1);
        }
        else if(attempts == 1)
        {
            h.appendChild(document.createTextNode("Hint 2:"))
            div.appendChild(h);
            div.appendChild(curHint.h2);
        }
        else if(attempts == 2)
        {
            h.appendChild(document.createTextNode("Hint 3:"))
            div.appendChild(h);
            div.appendChild(curHint.h3);
        }
        else
        {
            //document.getElementById("next").removeAttribute("disabled");
            //document.getElementById("submit").setAttribute("disabled", "disabled");
        }
            ////////disable submit, give answer///////
        attempts++;
    }
    
}

function displayMes(bool)
{
    var err = document.getElementById("error");
    var val = document.getElementById("valid");
    
    err.innerHTML = "";
    val.innerHTML = "";
    
    if(bool == null)
        error.appendChild(document.createTextNode("Error: Please Enter an Answer"));
    else if(bool == false)
        error.appendChild(document.createTextNode("Incorrect!"));
    else if(bool == true)
        valid.appendChild(document.createTextNode("Correct!"));
}

function getObj(cid, array)
{
    var i;
    for(i = 0; i < array.length; i++)
    {
        if(array[i].id == cid)
            return array[i];
    }
    return "";
}

function loadNull(obj, typ)
{
    if(JSON.parse(obj) == null)
    {
        if(typ == "str")
            return "";
        else if(typ == "num")
            return 0;
        else if(typ == "arr")
            return [];
        else 
            return null;
    }
    else
        return JSON.parse(obj);
}

function shuffleArray(array) {
    var i;
    for (i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function setupDataTable(obj)
{
    varVal = obj.varVal;
    varEq = obj.varEq;
    varPos = obj.varPos;
    varExtra = obj.varExtra;
    console.log("VARSWTUP");
    console.log(varVal);
    console.log(varEq);
    console.log(varExtra);
    var i, j;
    var dataDiv = document.getElementById("dataTable");
    var c = obj.columns;
    var r = obj.rows;
    var arr = obj.columnNames.split(",");
    var table= document.createElement("TABLE");
    table.setAttribute("class", "table table-striped table-condensed");
    var header = document.createElement("THEAD");
    var row = document.createElement("TR");
    var trial = document.createElement("TH");
    trial.innerHTML = "Trial";
    row.appendChild(trial);
    for(i = 0; i < arr.length; i++)
    {
        var th = document.createElement("TH");
        th.appendChild(document.createTextNode(arr[i]));
        row.appendChild(th);
    }
    header.appendChild(row);
    table.appendChild(header);
    var body = document.createElement("TBODY");
    for(i = 0; i < r; i++)
    {
        row = document.createElement("TR");
        var td = document.createElement("TD");
        td.appendChild(document.createTextNode((i+1)+": "));
        row.appendChild(td);
        for(j = 0; j < c; j++)
        {
            var id = obj.varPos[i][j];
            console.log(id);
            var t = document.createElement("TD");
            var input = document.createElement("LABEL");
            input.setAttribute("style", "width: 60%");
            //input.setAttribute("type", "text");
            input.setAttribute("id", id);
            //var units = document.createTextNode(qObj.units);
            t.appendChild(input);
            //t.appendChild(units);
            row.appendChild(t);
        }
        body.appendChild(row);
    }
    table.appendChild(body);
    dataDiv.appendChild(table);
    dataDiv.appendChild(document.createElement("BR"));
    var exp = document.createElement("BUTTON");
    exp.setAttribute("class", "btn btn-default");
    exp.setAttribute("style", "float:right");
    exp.innerHTML = "Export";
    exp.setAttribute("onclick", "exportData()");
    dataDiv.appendChild(exp);
    dataDiv.setAttribute("style", "display:block");
}

function dataVal(name, row)
{
    document.getElementById("error").innerHTML = "";
    document.getElementById("valid").innerHTML = "";
    var entries = document.getElementsByName(name);
    var valid = true;
    console.log(row);
    for(var i = 0; i < entries.length; i++) ////check all data is entered
    {
        if(entries[i].value == "")
        {
            document.getElementById("error").innerHTML = "Error: Please enter data";
            return;
        }
    }
    
    console.log("CHECK");
    var quest = getObj(curQuestion, questionArray);
    var dVar = quest.dVar[row];
    console.log(dVar);
    for(var j = 0; j < dVar.length; j++)
    {
        //var id = entries[j].getAttribute("id");
        //console.log(id);
        var dv = dVar[j]; ////is dVar nessacary?
        if(dv == "") /////no validation////
            continue;
        var data = parseFloat(entries[j].value);
        varVal[dv] = data;
        console.log(varVal);
        console.log(varEq[dv]);
        console.log(varVal[dv]);
        var ans = math.eval(varEq[dv], varVal);
        console.log(ans);
        if(ans)
        {
            console.log("TRUE");
            if(document.getElementById(dv) != null)
                document.getElementById(dv).innerHTML=data;
        }
        else if(!ans)
        {
            console.log("FALSE");
            console.log(ans);
            if(document.getElementById(dv) != null)
                document.getElementById(dv).innerHTML="";
            document.getElementById("error").innerHTML = "Error, data not valid";
            valid = false;
            displayHints();
        }
        else //////e
        {//////might put extras in here
        }
    }
    if(valid)
        document.getElementById("valid").innerHTML = "Correct!";       
}

function exportData()
{
    var csvContent = "data:text/csv;charset=utf-8,";
    var labels = dataObj.columnNames.split(",");
    for(var x = 0; x < labels.length; x++)
    {
        csvContent += labels[x];
        csvContent += ",";
    }
    csvContent += "\n";
    for(var i = 0; i < varPos.length; i++)
    {
        var row = varPos[i];
        for(var j = 0; j < row.length; j++)
        {
            var data;
            if(row[j] != "" && row[j] != "Infinity" && row[j] != "-Infinity")
                data = varVal[row[j]];
            else
                data = 0;
            csvContent += data;
            csvContent += ",";
        }
        csvContent += "\n";
    }
    console.log(csvContent);
    
    var encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
}

function fillData()
{
    for(var i in varVal)
    {
        if(varVal[i] != "Infinity" && varVal[i] != "-Infinity")
        {
            if(document.getElementById(i) != null)
                document.getElementById(i).innerHTML = varVal[i];
        }  
    }
}

function calcExtras()
{
    console.log(varExtra);
    for(var i in varExtra)
    {
        
        var data = varExtra[i];
        var ans = math.eval(varEq[data], varVal);
        if(!isNaN(ans) && ans != "Infinity" && ans != "-Infinity")
        {
            console.log(ans);
            document.getElementById(data).innerHTML = ans;
            varVal[data] = ans;
        }
    }
}

function savePDF(type)
{
    var jsonObj = {};
    var arr = [];
    jsonObj.section = sectionName;
    for(var i = 0; i < questionArray.length; i++)
    {
        var x = [questionArray[i].question, questionArray[i].attempts];
        arr.push(x);
    }
    
    if(type == "hypo")
    {
        arr.push(["Objective", hypoQuestions[0]]);
        arr.push(["Hypothesis", hypoQuestions[1]]);
        arr.push(["Variables", hypoQuestions[2]]);
        arr.push(["Experiment Outline", hypoQuestions[3]]);
        arr.push(["Chemical Hazards", hypoQuestions[4]]);
        
    }
    jsonObj.question = arr;
    
    console.log(jsonObj);
    
    var url = "/answers.pdf";
    var sendObj = JSON.stringify(jsonObj);
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST",url , true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(sendObj);
}

function displayHints()
{
    document.getElementById("hintsTab").setAttribute("class", "active");
    document.getElementById("pdfTab").setAttribute("class", "");
    document.getElementById("pdf").setAttribute("class", "tab-pane fade");
    document.getElementById("hintsBlock").setAttribute("class", "tab-pane fade in active");
    var div = document.getElementById("hints");
    var h = document.createElement("H4");
    console.log(attempts);
    console.log(curHint);
    console.log(curHint.h1);
    if(attempts == 0)
    {
        h.appendChild(document.createTextNode("Hint 1:"))
        div.appendChild(h);
        div.appendChild(curHint.h1);
    }
    else if(attempts == 1)
    {
        h.appendChild(document.createTextNode("Hint 2:"))
        div.appendChild(h);
        div.appendChild(curHint.h2);
    }
    else if(attempts == 2)
    {
        h.appendChild(document.createTextNode("Hint 3:"))
        div.appendChild(h);
        div.appendChild(curHint.h3);
    }
    else
    {
        //document.getElementById("next").removeAttribute("disabled");  
        //document.getElementById("submit").setAttribute("disabled", "disabled");
    }
            ////////disable submit, give answer///////
    attempts++;
}

