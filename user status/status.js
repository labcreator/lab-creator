var request = new XMLHttpRequest();
var assignments = [];
var docid = document.getElementById("status-body-table");

window.onload = function () {
    
//    var url = "statusexperiments.json";
        var url = "json/status.json";
    request.open("GET", url);
    
    console.log(request);
    request.onreadystatechange = function () {
        if((request.readyState === 1 || request.readyState === 4) && (request.status === 200 || request.status === 0)) {
            console.log('JSON parse : '+JSON.parse(request.responseText));
            assignments = JSON.parse(request.responseText);
            console.log(assignments);
            createexperiments(assignments);
        }
    }
    request.send();
    
    function createexperiments(assignments) {
        
        for(var i = 0; i < assignments.length; i++) {
           createtable(assignments[i]);
        }
    }
    
    function createtable(assignment) {
        
        var id = assignment.experimentId,
            name = assignment.experimentName;
        
        console.log('ID : '+id);
        console.log('NAME : '+name);
        
        var tr = document.createElement("tr"), td1, td2, td3, td4, td5, td6, a4, a5, a6, ele4, ele5, ele6;
        
        td1 = document.createElement("td");
        td1.appendChild(document.createTextNode(id));
        tr.appendChild(td1);
        
        td2 = document.createElement("td");
        td2.appendChild(document.createTextNode(name));
        tr.appendChild(td2);
        
        td3 = document.createElement("td");
        td3.appendChild(document.createTextNode(""));
        tr.appendChild(td3);
        
        td4 = document.createElement("td");
        ele4 = document.createTextNode("prelab");
        a4 = document.createElement("a")
        a4.setAttribute("href", name+"/prelab");
        a4.appendChild(ele4);
        td4.appendChild(a4);
        tr.appendChild(td4);
        
        td5 = document.createElement("td");
        ele4 = document.createTextNode("lab");
        a4 = document.createElement("a")
        a4.setAttribute("href", name+"/lab");
        a4.appendChild(ele4);
        td5.appendChild(a4);
        tr.appendChild(td5);
        
        td6 = document.createElement("td");
        ele4 = document.createTextNode("post lab");
        a4 = document.createElement("a")
        a4.setAttribute("href", name+"/postlab");
        a4.appendChild(ele4);
        td6.appendChild(a4);
        tr.appendChild(td6);
        
        docid.appendChild(tr);
//        return tr;
    }
}